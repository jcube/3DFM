#include "NSICustomExporter.h"

#include <maya/MObjectHandle.h>

#include <vector>


class XGenSplinesDelegate : public NSICustomDelegate
{
public:
	XGenSplinesDelegate(
		MObject i_node, 
		NSIContext_t i_nsiContext,
		const MString& i_handle, 
		NSIUtils& i_utils,
		void* i_data );

	virtual ~XGenSplinesDelegate();

	virtual void Create();
	virtual void SetAttributes();
	virtual void SetAttributesAtTime( double i_tem, bool i_no_motion );

	virtual void Connect();

	virtual bool IsDeformed() const { return true; }
	virtual bool RegisterCallbacks();
	virtual void RemoveCallbacks();
	virtual void OverrideAttributes( MObject& i_attributes );

private:
	static void AttributeChangedCB(
		MNodeMessage::AttributeMessage i_msg,
		MPlug& i_plug,
		MPlug& i_otherPlug,
		void* i_delegate);

	static void NodeDirtyCB(MObject&,	MPlug& i_plug, void* i_delegate);

	/**
		\brief Adds a Maya callback ID associated with this delegate
	*/
	void AddCallbackId( MCallbackId id )
	{
		m_ids.push_back( id );
	}

	/**
		\brief Returns true if the specified xgen node is used as a guide 
	*/
	bool IsUsedAsGuide( const MFnDependencyNode& i_depFn ) const;

	bool IsCubic()const;

	/*
		Two flags to output a newly created node during live render at the right 
		time.

		Trying to output the XGen description before it is fully initialized may
		produce errors and will disrupt the new description disply in the Viewport.

		See AttributeChangedCB for details. 
	*/
	bool m_readyForOutput;
	bool m_initialOutputIsDone;
	bool m_exportedN;

	std::vector<MCallbackId> m_ids;
	MObjectHandle m_overrides; 
};
