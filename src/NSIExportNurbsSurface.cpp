
#include "NSIExportNurbsSurface.h"
#include "NSIExportMesh.h"

#include <nsi.hpp>

#include <maya/MFnNurbsSurface.h>
#include <maya/MStatus.h>
#include <maya/MTesselationParams.h>
#include <maya/MIntArray.h>
#include <assert.h>

NSIExportNurbsSurface::NSIExportNurbsSurface(
	MFnDagNode &i_dag,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( i_dag, i_context ),
	m_delegate( 0x0 )
{
	MFnNurbsSurface nurbs( Object() );

	MTesselationParams tp(
		MTesselationParams::kStandardFitFormat,
		MTesselationParams::kQuads );

	MStatus status;
	m_trs = nurbs.tesselate(
		MTesselationParams::fsDefaultTesselationParams,
		MObject::kNullObj, &status );

	if( status != MStatus::kSuccess && m_trs.hasFn(MFn::kTransform) )
		return;

	MFnDagNode dag( m_trs );

	if( dag.childCount()!=1 )
		return;

	m_mesh = dag.child(0, &status);

	if( status == MStatus::kSuccess )
	{
		m_delegate = new NSIExportMesh( i_dag, m_mesh, i_context );
	}
}

NSIExportNurbsSurface::~NSIExportNurbsSurface()
{
	MFnDagNode dag( m_trs );
	dag.removeChild( m_mesh );

	MFnDagNode parent( dag.parent(0) );
	parent.removeChild( m_trs );

	delete m_delegate;
}

/**
	\brief A no-op method because everyhing is done in the mesh delegate
*/
void NSIExportNurbsSurface::Create()
{
	if( m_delegate )
	{
		m_delegate->Create();
	}
}

/**
	\brief A no-op method because everyhing is done in the mesh delegate
*/
void NSIExportNurbsSurface::SetAttributes( void )
{
	if( !m_delegate )
		return;

	m_delegate->SetAttributes();
}

/**
	\brief A no-op method because everything is done in the mesh delegate
*/
void NSIExportNurbsSurface::SetAttributesAtTime(
	double i_time, bool i_no_motion )
{
	if( !m_delegate )
		return;

	m_delegate->SetAttributesAtTime( i_time, i_no_motion );
}

