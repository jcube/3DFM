/*
  Copyright (c) 2014 soho vfx inc.
  Copyright (c) 2014 The 3Delight Team.
*/

#include "DL_externalCommandServer.h"

#include <maya/MGlobal.h>

#include <3Delight/Feedback.h>

namespace
{
void ProcessFeedback(void *userdata, const char *message)
{
	/* Currently, the json will not contain ''' so no need to escape that. */
	MString cmd{"DL_feedback.HandleFeedback(r'''"};
	cmd += message;
	cmd += "''')";
	MGlobal::executePythonCommandOnIdle(cmd);
}
}

void DL_externalCommandServer::CleanUp()
{
	DlFeedbackRemoveHandler();
}

MString DL_externalCommandServer::GetServerID()
{
	DlFeedbackSetHandler(&ProcessFeedback, nullptr);
	return DlFeedbackGetID();
}
