#ifndef __NSIExportLightGroup_h
#define __NSIExportLightGroup_h

#include "NSIExportDelegate.h"

/**
	\sa NSIExportDelegate
*/
class NSIExportLightGroup : public NSIExportDelegate
{
public:
	NSIExportLightGroup(
		MObject& i_obj,
		const NSIExportDelegate::Context& i_context )
	:
		NSIExportDelegate( i_obj, i_context )
	{
	}

	virtual void Create();
	virtual void SetAttributes();
	virtual void SetAttributesAtTime( double, bool );
	virtual void Connect( const DelegateTable *i_dag_hash );
private:
};
#endif
