/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#include "VPUtilities.h"

#include <maya/MDrawRegistry.h>
#include <maya/MFragmentManager.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MShaderManager.h>

#include "DL_utils.h"
#include "VPShaderOverride.h"
#include "VPSurfaceShaderOverride.h"

#include <filesystem>

namespace VPUtilities
{

static MHWRender::MFragmentManager* GetFragmentManager()
{
	MHWRender::MRenderer* renderer = MHWRender::MRenderer::theRenderer();
	if( !renderer )
		return 0x0;

	MHWRender::MFragmentManager* fragmentMgr = renderer->getFragmentManager();
	return fragmentMgr;
}

MString GetUserFragmentPath()
{
	MString path;

	char* userPath = getenv( "_3DFM_USER_FRAGMENT_PATH" );

	std::error_code ec;
	if( userPath && std::filesystem::is_directory(userPath, ec) )
	{
		path = userPath;
	}

	return path;
}

MString GetBuiltinFragmentPath()
{
	return getDelightPath() + MString( "/maya/shaderFragments" );
}

void AddFragmentPath( const MString& i_path )
{
	MHWRender::MFragmentManager* fragmentMgr = GetFragmentManager();
	if( fragmentMgr && i_path.length() > 0 )
	{
		fragmentMgr->addFragmentPath( i_path );
	}
}

MString AddFragment( const MString& i_name, bool i_isGraph )
{
	MString fragment;
	MHWRender::MFragmentManager* fragmentMgr = GetFragmentManager();
	if( fragmentMgr )
	{
		if( i_isGraph )
		{
			fragment = fragmentMgr->addFragmentGraphFromFile( i_name + ".xml" );
		}
		else
		{
			fragment = fragmentMgr->addShadeFragmentFromFile( i_name + ".xml", false );
		}
	}

	return fragment;
}

bool RemoveFragment( const MString& i_name )
{
	MHWRender::MFragmentManager* fragmentMgr = GetFragmentManager();
	if( !fragmentMgr )
	{
		return false;
	}

	return fragmentMgr->removeFragment( i_name );
}

bool FragmentExists( const MString& i_name )
{
	MHWRender::MFragmentManager* fragmentMgr = GetFragmentManager();
	if( fragmentMgr && fragmentMgr->hasFragment( i_name ) )
		return true;

	return false;
}

void ClearShaderMgrEffectCache()
{
#if MAYA_API_VERSION >= 201700
	MHWRender::MRenderer* renderer = MHWRender::MRenderer::theRenderer();
	if( !renderer )
		return;

	/*
		In order for the remove operation to affect the VP rendering, the shader
		manager effect cache must be cleared and the shader override must be re-
		registered.
	*/
	const MHWRender::MShaderManager* shaderMgr = renderer->getShaderManager();
	if( shaderMgr )
	{
		shaderMgr->clearEffectCache();
	}
#endif
}

MString GetFragmentName( const MString& i_nodeTypeName )
{
	MString noFragment( "" );
	MHWRender::MFragmentManager* fragmentMgr = GetFragmentManager();
	if( !fragmentMgr )
	{
		return noFragment;
	}

	MString graphName = i_nodeTypeName + "FragmentGraph";
	if( fragmentMgr->hasFragment( graphName ) )
	{
		return graphName;
	}

	MString fragmentName = i_nodeTypeName + "Fragment";
	if( fragmentMgr->hasFragment( fragmentName ) )
	{
		return fragmentName;
	}

	return noFragment;
}

void GetFragmentList( MStringArray& o_list )
{
	o_list.clear();
	MHWRender::MFragmentManager* fragmentMgr = GetFragmentManager();
	if( !fragmentMgr )
		return;

	fragmentMgr->fragmentList( o_list );
}

MStatus GetFragmentContent( const MString& i_fragmentName, MString& o_buffer )
{
	MHWRender::MFragmentManager* fragmentMgr = GetFragmentManager();
	if( !fragmentMgr )
		return MStatus::kFailure;

	bool retVal = fragmentMgr->getFragmentXML( i_fragmentName, o_buffer );

	if( !retVal )
		return MStatus::kFailure;

	return MStatus::kSuccess;
}

MStatus GetFragmentContent(
	const MObject& i_shadingNode,
	MString& o_buffer,
	bool i_includeUpstreamNodes,
	MDagPath* i_objContext )
{
	MHWRender::MFragmentManager* fragmentMgr = GetFragmentManager();
	if( !fragmentMgr )
		return MStatus::kFailure;

	bool retVal = fragmentMgr->getFragmentXML(
		i_shadingNode, o_buffer, i_includeUpstreamNodes, i_objContext );

	if( !retVal )
		return MStatus::kFailure;

	return MStatus::kSuccess;
}

void SetEffectOutputDir(const MString& i_dir)
{
	MHWRender::MFragmentManager* fragmentMgr = GetFragmentManager();
	if( !fragmentMgr )
		return;

	fragmentMgr->setEffectOutputDirectory( i_dir );
}

void SetGraphOutputDir(const MString& i_dir)
{
	MHWRender::MFragmentManager* fragmentMgr = GetFragmentManager();
	if( !fragmentMgr )
		return;

	fragmentMgr->setIntermediateGraphOutputDirectory( i_dir );
}

static MString GetVPOverrideClassPrefix()
{
	return MString( "drawdb/shader/surface/dl/" );
}

MString GetVPShaderOverrideClass()
{
	return GetVPOverrideClassPrefix() + "VPShader";
}

MString GetVPSurfaceShaderOverrideClass()
{
	return GetVPOverrideClassPrefix() + "VPSurfaceShader";
}

void RegisterVPShaderOverride()
{
	MHWRender::MDrawRegistry::registerShadingNodeOverrideCreator(
		GetVPShaderOverrideClass(),
		MString("3Delight for Maya"),
		VPShaderOverride::creator);
}

void DeregisterVPShaderOverride()
{
	MHWRender::MDrawRegistry::deregisterShadingNodeOverrideCreator(
		GetVPShaderOverrideClass(),
		MString("3Delight for Maya") );
}

void RegisterVPSurfaceShaderOverride()
{
	MHWRender::MDrawRegistry::registerSurfaceShadingNodeOverrideCreator(
		GetVPSurfaceShaderOverrideClass(),
		MString("3Delight for Maya"),
		VPSurfaceShaderOverride::creator);
}

void DeregisterVPSurfaceShaderOverride()
{
	MHWRender::MDrawRegistry::deregisterSurfaceShadingNodeOverrideCreator(
		GetVPSurfaceShaderOverrideClass(),
		MString("3Delight for Maya") );
}

} // VPUtilities namespace

