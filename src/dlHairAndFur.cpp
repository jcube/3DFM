#include "dlHairAndFur.h"

#include <maya/MFloatVector.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MObjectArray.h>

#include "DL_autoLoadOSL.h"
#include "DL_utils.h"
#include "OSLUtils.h"

#include <cassert>

MObject dlHairAndFur::s_color;
MObject dlHairAndFur::s_outColor;

void* dlHairAndFur::creator()
{
	return new dlHairAndFur();
}

MStatus dlHairAndFur::initialize()
{
	MStringArray shaderPaths = OSLUtils::GetBuiltInSearchPaths();
	MString shaderName( "dlHairAndFur" );
	DlShaderInfo *info;

	OSLUtils::OpenShader( shaderName, shaderPaths, info );

	MObjectArray objects;
	MStringArray objectNames;

	DL_OSLShadingNode::CreateAttributesFromShaderParameters(
		info, 0x0, &objects, &objectNames );

	/*
		This node used to be built manually and unfortunately had parameters
		with odd short names. We'll manually replace those here as the above
		function only produces short names identical to long names and I don't
		think it should support anything else. Failure to match the old short
		names causes read errors in existing scenes.
	*/
	for( unsigned i = 0; i < objects.length() && i < objectNames.length(); ++i )
	{
		if( objectNames[i] == "color" )
		{
			MFnNumericAttribute nAttr;
			objects[i] = nAttr.createColor("color", "c");
			Utilities::makeShaderInputAttribute(nAttr);
			nAttr.setDefault(0.8f, 0.3f, 0.3f);
			Utilities::setNiceNameColor(nAttr, "Dye Color");
		}
		else if( objectNames[i] == "outColor" )
		{
			MFnNumericAttribute nAttr;
			objects[i] = nAttr.createColor("outColor", "oc");
			Utilities::makeOutputAttribute(nAttr);
		}
	}

	if(
		!DL_OSLShadingNode::FindAttribute(
			"color", s_color, objects, objectNames)
		|| !DL_OSLShadingNode::FindAttribute(
			"outColor", s_outColor, objects, objectNames) )
	{
		assert(false);
		// It makes no sense to return success here, but if we don't, Maya crashes.
		return MStatus::kSuccess;
	}

	for( unsigned i = 0; i < objects.length(); i++ )
	{
		addAttribute( objects[ i ] );
	}

	attributeAffects( s_color, s_outColor );

	MString name = info->shadername().c_str();
	MString niceName = OSLUtils::GetShaderNiceName( info );
	DL_OSLShadingNode::DefineShaderNiceName( name, niceName );

	return MStatus::kSuccess;
}

void dlHairAndFur::postConstructor()
{
	MFnDependencyNode dep_node(thisMObject());

	/* Set the default node name */
	MString defaultname = typeName() + "#";

	/* Save the digit in the node name */
	if( isdigit( defaultname.asChar()[0] ) )
	{
		defaultname = "_" + defaultname;
	}

	dep_node.setName(defaultname);

	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlHairAndFur::compute(
    const MPlug& i_plug,
    MDataBlock& i_block )
{
    fprintf(stderr, "dlHairAndFur::compute\n");
	if ((i_plug != s_outColor) && (i_plug.parent() != s_outColor))
	{
		return MS::kUnknownParameter;
	}

	/* Just transfer s_color to s_outColor */
	MFloatVector& color = i_block.inputValue( s_color ).asFloatVector();

	/* set ouput color attribute */
  	MDataHandle outColorHandle = i_block.outputValue( s_outColor );
	MFloatVector& outColor = outColorHandle.asFloatVector();
	outColor = color;
	outColorHandle.setClean();

	return MS::kSuccess;
}
