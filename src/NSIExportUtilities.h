#ifndef __NSIExportUtilities_h
#define __NSIExportUtilities_h

#include <vector>

#include <maya/MFn.h>
#include <maya/MObject.h>
#include <maya/MObjectArray.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MDagPath;
class MIntArray;
class MFnCamera;
class MFnDagNode;
class MFnNumericData;
class MFnSet;
class MIntArray;
class MString;
class MPlug;
#endif

#include "dlVec.h"
#include "nsi.hpp"

#include <string>
#include <unordered_set>

class RenderPassInterfaceForScreen;
struct ParamData;
struct ShaderData;

namespace NSIExportUtilities
{
	/**
		\brief A NSI Argument for a MStringArray.
	*/
	class MStringArrayNSIArg : public NSI::ArgBase
	{
	public:
		MStringArrayNSIArg(
			const std::string &i_name, const MStringArray& i_array );
		~MStringArrayNSIArg();

		virtual void FillNSIParam( NSIParam_t& p ) const;

	private:
		int m_arraySize;
		char** m_array;
	};

	/**
		\brief Returns the list of items in a lightGroup that are light sources.

		\param i_lightGroup
			The lightGroup node to inspect
		\param i_flatten
			When true, items that are lightGroup are recursively inspected for light
			sources.
			When false, items that are lightGroups are ignored.
		\param o_items
			The list of light sources, as MObjects.
	*/
	void GetLightsFromLightGroup(
		MObject i_lightGroup,
		bool i_flatten,
		MObjectArray& o_items );

	/**
		\brief Flattens a Maya MFnSet so to have a map of basic type (no
		transforms, and no sets)

		\param i_set
			The Maya set to flatten.
		\param i_types
			The object types to find.
		\param i_live
			True if we're exporting for a live/IPR render.
		\param o_nsi_handls
			The resulting object handles.
	*/
	void FlattenMayaSet(
		const MFnSet &i_set,
		const MIntArray &i_types,
		bool i_live,
		std::unordered_set<std::string> &o_nsi_handles );

	/**
		\brief Flattens a Maya LightGroup (2018 and up)

		There is no filtering done on light group items.

		\param i_set
			The Maya light group to flatten.
		\param i_live
			True if we're exporting for a live/IPR render.
		\param o_nsi_handles
			The resulting object handles.
	*/
	void FlattenMayaLightGroup(
		MObject i_lightGroup,
		bool i_live,
		std::unordered_set<std::string>& o_nsi_handles );

	/**
		\brief Returns all objects under a Maya transform.

		\param i_root
			The root of the search, must be a transform.

		\param i_type
			The objects types to search for.

		\param i_live
			True if we're exporting for a live/IPR render.

		\param o_nsi_handles
			Returns the list of NSI handles

		\param o_objects
			Will contain the object paths.
	*/
	void FlattenMayaTree(
		const MFnDagNode &i_root,
		MIntArray &i_types,
		bool i_live,
		std::unordered_set<std::string> &o_nsi_handles,
		std::vector<MObject> &o_objects );

	/**
		\brief Returns all objects under a Maya transform.

		\param i_root
			The root of the search, must be a transform.

		\param i_type
			The objects types to search for.

		\param o_objects
			Will contain the object paths.

		This is a simpler version of FlattenMayaTree declared above, without the
		NSI handles output.
	*/
	void FlattenMayaTree(
		const MFnDagNode &i_root,
		MIntArray &i_types,
		std::vector<MObject> &o_objects );

	/**
		\brief Returns true if the MObject is a shading node.
	*/
	bool IsShadingNode( MObject i_node );

	/**
		\brief Returns true if i_plug is a child of an array of compound attributes.

		This basically returns true when dealing with a plug that contains one value
		of one of the ramp parameters.

		See the comment of ArrayOfCompoundNumValuesAsNSIAttr in the .cpp
		for a bit more about this.
	*/
	bool IsChildOfArrayOfCompoundAttr( const MPlug& i_plug );

	/**
		\brief Returns the child index of a compound child attribute.

		Returns -1 when the search fails.
	*/
	int CompoundAttrChildIndex(const MPlug& i_plug );

	/**
		\brief Returns true if i_plug is a color attribute.
	*/
	bool IsColorAttr( const MPlug& i_plug );

	/**
		Used to specify if the Maya attribute is to be considered as an input
		attribute or an output attribute.
	*/
	enum IoType
	{
		eInput,
		eOutput
	};

	/**
		\brief Returns the attribute name string formatted as expected by the
		shader data cache.

		The returned name for a single Maya attribute will vary according to
		i_type; the shader data cache may contain one input and one output OSL
		parameter associated to a single Maya attribute.

		\param i_plug
			The Maya attribute

		\param i_type
			Specifies if the Maya attribute should be considered an input or an
			output attribute.
	*/
	MString GetShadingNodeAttributeName( const MPlug& i_plug, IoType i_type );

	const ShaderData* GetShaderData( const MObject& i_object );
	/**
		\brief
		Returns the OSL shader parameter data, if any, that corresponds to the
		Maya plug for the specified i_type.

		A null pointer means that no corresponding OSL shader parameter was found.

		This will read and cache the OSL shader parameter data if the shader data
		cache does not contain yet info about the shader associated with the
		type of i_plug.node().

		The shader data cache may contain one input and one output OSL
		parameter associated to a single Maya attribute.

		\param i_plug
			The Maya node & attribute for which an OSL shader & parameter will be
			searched for.

		\param i_type
			Specifies if the Maya attribute should be considered an input or an
			output attribute.
	*/
	const ParamData* GetShaderParameterData( const MPlug& i_plug, IoType i_type );

	/**
		\brief
		A utility function that returns the shader parameter name as MString from
		the shader data returned by GetShaderParameterData.

		\param i_plug
			The Maya node & attribute for which an OSL shader & parameter will be
			searched for.

		\param i_type
			Specifies if the Maya attribute should be considered an input or an
			output attribute.
	*/
	MString GetShaderParameterName( const MPlug& i_plug, IoType i_type );

	/**
		\brief Output a Maya shading node attribute as a NSI attribute.

		Returns the pointer to the cached shader param data, or null if no shader
		parameter matches.

		\param i_plug
			The plug designating the Maya attribute to output.

		\param i_type
			Specifies if the Maya attribute should be considered an input or an
			output attribute.

		\param i_shaderData
			The optional pointer to the cached shader data.

		\param i_live
			True if we're exporting for a live/IPR render.

		\param o_argList
			The NSI ArgumentList to which the NSI attribute will be appended.
	*/
	const ParamData* MayaShaderAttributeToNSIAttribute(
		const MPlug& i_plug,
		IoType i_type,
		const ShaderData* i_shaderData,
		bool i_live,
		NSI::ArgumentList& o_argList );

	/**
		\brief Computes what is needed for "screenwindow" camera attribute.

		Taken from MEL's outputCameraProjection and adapted to C++.
	*/
	float ComputeFieldOfView(
		const MFnCamera &cam);
	void ComputeScreenSizeAndCenter(
		const MFnCamera &cam,
		const dl::V2i &res,
		dl::V2f &size,
		dl::V2f &center );
	void ComputeRollAndTranslate(
		const MFnCamera &cam,
		dl::V2f &size,
		dl::V2f &center );
	void ComputePanAndZoom(
		const MFnCamera &cam,
		const dl::V2i &res,
		dl::V2f &size,
		dl::V2f &center);


	/**
		\brief Read and convert a color space attribute.

		This reads a color space attribute from a maya node and converts it to
		a name 3Delight understands.

		\param i_node
			The dependency node on which the attribute is stored.
		\param i_attribute_name
			The name of the color space attribute.
		\returns
			A color space string for use by 3Delight. If none was found on the
			node, "auto" is returned. 3Delight understands this too.
	*/
	std::string ReadColorSpace(
		const MObject &i_node,
		const MString &i_attribute_name );

	/**
		\brief Returns the full name of the attribute represented by a plug
		given MPlug.
	*/
	MString AttributeName( const MPlug &i_plug );

	/**
		\brief Exports an NSI screen node.
	*/
	void ExportScreen(
		NSIContext_t i_context,
		const RenderPassInterfaceForScreen& i_render_pass,
		const MFnCamera& i_camera,
		const MString& i_screen_handle,
		bool i_apply_display_options,
		bool i_live_render );
}

#endif
