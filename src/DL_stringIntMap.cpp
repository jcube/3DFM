#include <maya/MStringArray.h>

#include "DL_stringIntMap.h"

DL_stringIntMap::DL_stringIntMap()
{
  // nothing else needed
}

DL_stringIntMap::~DL_stringIntMap()
{
  removeAllMaps();
}

void
DL_stringIntMap::removeAllMaps()
{
  m_hashTable.clear();
}

void
DL_stringIntMap::clearAllMaps()
{
  t_tableType::iterator iter;

  for (iter = m_hashTable.begin(); iter != m_hashTable.end(); ++iter)
  {
    iter->second.m_ints.clear();
    iter->second.m_strings.clear();
    iter->second.m_arrays.clear();
  }
}

bool
DL_stringIntMap::clearMap(const MString& map_name)
{
  t_tableType::iterator iter = m_hashTable.find(map_name);

  if (iter == m_hashTable.end())
    return false;

  iter->second.m_ints.clear();
  iter->second.m_strings.clear();
  iter->second.m_arrays.clear();

  return true;
}

bool
DL_stringIntMap::addMap(const MString& map_name)
{
  bool success;

  if (!containsMap(map_name))
  {
    // creates an entry in the hash table for this map
    // TODO: is there a more clear way to do this?
    m_hashTable[map_name];

    success = true;
  }
  else
    success = false;

  return success;
}

bool
DL_stringIntMap::removeMap(const MString& map_name)
{
  bool                  success;
  t_tableType::iterator iter;

  iter = m_hashTable.find(map_name);

  if (iter != m_hashTable.end())
  {
    m_hashTable.erase(iter);
    success = true;
  }
  else
    success = false;

  return success;
}

bool
DL_stringIntMap::containsMap(const MString& map_name)
{
  bool                  contains_map;
  t_tableType::iterator iter;

  iter = m_hashTable.find(map_name);

  if (iter != m_hashTable.end())
    contains_map = true;
  else
    contains_map = false;

  return contains_map;
}

bool
DL_stringIntMap::set(const MString& map_name, const MString& key, int value)
{
  t_tableType::iterator iter = m_hashTable.find(map_name);

  if (iter == m_hashTable.end())
    return false;

  iter->second.m_ints[key] = value;

  return true;
}

bool
DL_stringIntMap::set(const MString& map_name,
                     const MString& key,
                     const MString& value)
{
  t_tableType::iterator iter = m_hashTable.find(map_name);

  if (iter == m_hashTable.end())
    return false;

  iter->second.m_strings[key] = value;

  return true;
}

bool
DL_stringIntMap::set(
  const MString& map_name,
  const MString& key,
  const MStringArray& value)
{
  t_tableType::iterator iter = m_hashTable.find(map_name);

  if (iter == m_hashTable.end())
    return false;

  iter->second.m_arrays[key] = value;

  return true;
}

bool
DL_stringIntMap::get(int& value, const MString& map_name, const MString& key)
{
  t_tableType::iterator hash_iter = m_hashTable.find(map_name);

  if (hash_iter == m_hashTable.end())
    return false;

  t_intMapType::iterator map_iter = hash_iter->second.m_ints.find(key);

  if (map_iter == hash_iter->second.m_ints.end())
    return false;

  value = map_iter->second;
  return true;
}

bool
DL_stringIntMap::get(MString& value,
                     const MString& map_name,
                     const MString& key)
{
  t_tableType::iterator hash_iter = m_hashTable.find(map_name);

  if (hash_iter == m_hashTable.end())
    return false;

  t_stringMapType::iterator map_iter = hash_iter->second.m_strings.find(key);

  if (map_iter == hash_iter->second.m_strings.end())
    return false;

  value = map_iter->second;
  return true;
}

bool
DL_stringIntMap::get(
  MStringArray& value,
  const MString& map_name,
  const MString& key)
{
  t_tableType::iterator hash_iter = m_hashTable.find(map_name);

  if (hash_iter == m_hashTable.end())
    return false;

  t_arrayMapType::iterator map_iter = hash_iter->second.m_arrays.find(key);

  if (map_iter == hash_iter->second.m_arrays.end())
    return false;

  value = map_iter->second;
  return true;
}

bool
DL_stringIntMap::mapContains(const MString& map_name, const MString& key)
{
  t_tableType::iterator hash_iter = m_hashTable.find(map_name);

  if (hash_iter == m_hashTable.end())
    return false;

  return
    hash_iter->second.m_ints.find(key) != hash_iter->second.m_ints.end() ||
    hash_iter->second.m_strings.find(key) != hash_iter->second.m_strings.end()||
    hash_iter->second.m_arrays.find(key) != hash_iter->second.m_arrays.end();
}

bool
DL_stringIntMap::removeKeyInt(const MString& map_name, const MString& key)
{
  t_tableType::iterator hash_iter = m_hashTable.find(map_name);

  if (hash_iter == m_hashTable.end())
    return false;

  t_intMapType::iterator int_it = hash_iter->second.m_ints.find(key);
  if( int_it != hash_iter->second.m_ints.end() )
  {
    hash_iter->second.m_ints.erase( int_it );
    return true;
  }

  return false;
}

bool
DL_stringIntMap::removeKeyString(const MString& map_name, const MString& key)
{
  t_tableType::iterator hash_iter = m_hashTable.find(map_name);

  if (hash_iter == m_hashTable.end())
    return false;

  t_stringMapType::iterator str_it = hash_iter->second.m_strings.find(key);
  if( str_it != hash_iter->second.m_strings.end() )
  {
    hash_iter->second.m_strings.erase( str_it );
    return true;
  }

  return false;
}

bool
DL_stringIntMap::getAllKeys(MStringArray& keys,
                              const MString& map_name)
{
  keys.clear();

  t_tableType::iterator hash_iter = m_hashTable.find(map_name);

  if (hash_iter == m_hashTable.end())
    return false;

  combinedMaps& maps = hash_iter->second;

  for(
    t_intMapType::iterator map_iter = maps.m_ints.begin();
    map_iter != maps.m_ints.end();
    ++map_iter )
  {
    keys.append( map_iter->first );
  }

  for(
    t_stringMapType::iterator map_iter = maps.m_strings.begin();
    map_iter != maps.m_strings.end();
    ++map_iter )
  {
    keys.append( map_iter->first );
  }

  for(
    t_arrayMapType::iterator map_iter = maps.m_arrays.begin();
    map_iter != maps.m_arrays.end();
    ++map_iter )
  {
    keys.append( map_iter->first );
  }

  return true;
}
