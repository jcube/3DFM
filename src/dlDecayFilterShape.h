/*
  Copyright (c) 2018 The 3Delight Team.
*/

#ifndef __dlDecayFilterShape_h__
#define __dlDecayFilterShape_h__

#include <maya/MPxSurfaceShape.h>

class dlDecayFilterShape : public MPxSurfaceShape
{
  public:
    dlDecayFilterShape();
    virtual ~dlDecayFilterShape(); 

    virtual void postConstructor();
    virtual MStatus compute( const MPlug&, MDataBlock& );
    virtual MStatus shouldSave(const MPlug &i_plug, bool &o_isSaving);

    static void* creator();
    static MStatus initialize();

    static MTypeId id;

  private:

    // Input attributes
    static MObject s_decayType;
    static MObject s_decayRange;    
    static MObject s_decayRangeStart;
    static MObject s_decayRangeEnd;
    static MObject s_decayCurve;
    // Output attribute
    static MObject s_filter_output;
};

#endif
