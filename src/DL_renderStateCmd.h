#ifndef __DL_RENDERSTATECMD_H__
#define __DL_RENDERSTATECMD_H__

#include <maya/MPxCommand.h>

class DL_renderStateCmd : public MPxCommand
{
public:
  virtual MStatus       doIt(const MArgList& args);

  virtual bool          isUndoable() const
                        { return false; }

  static void*          creator()
                        { return new DL_renderStateCmd; }

  static MSyntax        newSyntax();
private:

};

#endif
