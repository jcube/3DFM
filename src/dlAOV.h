#ifndef __dlAOV_H__
#define __dlAOV_H__

#include <maya/MPxNode.h>

/*
	Common attributes expected t6o be defined on all aov nodes
*/
struct dlAOVCommonAttrs
{
	MStatus initialize();

	MObject m_name;
	MObject m_source;
	MObject m_type;
};

/*
	Node for primitive attribute AOVs
*/
class dlAOVPrimitiveAttribute : public MPxNode
{
public:
	static void* creator();

	static MStatus initialize();

	virtual void postConstructor();
	virtual MStatus compute( const MPlug& i_plug, MDataBlock& i_dataBlock);

private:
	static dlAOVCommonAttrs m_common;
};

/*
	Node for some single input AOV
*/
class dlAOV : public MPxNode
{
public:
	static void* creator();

	static MStatus initializeFloat();
	static MStatus initializeColor();

	virtual void postConstructor();
	virtual MStatus compute( const MPlug& i_plug, MDataBlock& i_dataBlock);

private:
	static dlAOVCommonAttrs m_common;
	static MObject m_defaultValue;
};


/*
	Node for a group of aovs
*/
class dlAOVGroup : public MPxNode
{
public:
	static void* creator();

	static MStatus initialize();

	virtual void postConstructor();
	virtual MStatus compute( const MPlug& i_plug, MDataBlock& i_dataBlock);

private:
	static MObject m_colorAOVs;
	static MObject m_colorAOVValues;
	static MObject m_colorAOVNames;

	static MObject m_floatAOVs;
	static MObject m_floatAOVValues;
	static MObject m_floatAOVNames;

	static MObject m_outColor;
};

#endif
