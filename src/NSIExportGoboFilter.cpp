#include "NSIExportGoboFilter.h"

#include "nsi.hpp"

NSIExportGoboFilter::NSIExportGoboFilter(
	MObject i_object,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportShader( i_object, i_context )
{
}

void NSIExportGoboFilter::SetAttributes()
{
	NSIExportShader::SetAttributes();
	NSI::Context nsi( m_nsi );

	MFnDagNode dagNode(Object());
	MString parentName;
	if (dagNode.parentCount() > 0)
	{
		MObject parent = dagNode.parent(0);
		parentName = NSIExportDelegate::NSIHandle(parent, m_live);
	}
	else
	{
		parentName = NSIExportDelegate::NSIHandle(dagNode, m_live);
	}
	nsi.SetAttribute(
		m_nsi_handle,
		NSI::StringArg("filterCoordinateSystem", parentName.asChar()));
}
