#include <stdlib.h>

#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MIntArray.h>

#include "DL_about.h"

/* This is a 3Delight header. */
#include "delight.h"

#include <string.h>

MSyntax
DL_about::newSyntax()
{
  MSyntax syntax;

  syntax.addFlag("-bd", "-buildDate");
  syntax.addFlag("-cs", "-copyrightString");
  syntax.addFlag("-dvs", "-delightLibVersionString");
  syntax.addFlag("-if", "-isFree");
  syntax.addFlag("-rvs", "-rendererVersionString");
  syntax.addFlag("-rvi", "-rendererVersionIntArray");
  syntax.addFlag("-rd", "-rendererDescription");
  
  return syntax;
}

MStatus
DL_about::doIt(const MArgList& args)
{
  MStatus         status;
  MArgDatabase    argData(syntax(), args, &status);

  if( !status )
    return status;

  if(argData.isFlagSet("-buildDate"))
  {
    setResult(MString(__DATE__));
  }
  else if(argData.isFlagSet("-copyrightString"))
  {
    setResult(MString(DlGetCopyrightString()));
  }
  else if(argData.isFlagSet("-delightLibVersionString"))
  {
    setResult(MString(DlGetVersionString()));
  }
  else if(argData.isFlagSet("-isFree"))
  {
    setResult(DlIsFreeLibrary());
  }
  else if( argData.isFlagSet( "-rvs" ) )
  {
    char *version = strdup( DlGetVersionString() );

    /* Cut string at first space to keep only version number. */
    char *space = strchr( version, ' ' );
    if( space )
      *space = '\0';

    setResult( version );
    free( version );
  }
  else if( argData.isFlagSet( "-rvi" ) )
  {
    MString dlVersion(DlGetVersionString());
    int major = 0;
    int minor = 0;
    int patch = 0;

    sscanf( dlVersion.asChar(), "%d.%d.%d", &major, &minor, &patch );

    MIntArray numbers;
    numbers.append( major );
    numbers.append( minor );
    numbers.append( patch );
    
    setResult( numbers );
  }
  else if( argData.isFlagSet( "-rd" ) )
  {
    setResult( DlGetLibNameAndVersionString() );
  }

  return status;
}
