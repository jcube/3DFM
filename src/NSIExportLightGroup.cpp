#include "NSIExportLightGroup.h"

#include "NSIExportUtilities.h"

#include <nsi.hpp>

void NSIExportLightGroup::Create()
{
	NSICreate( m_nsi, m_nsi_handle, "set", 0, 0x0 );
}

void NSIExportLightGroup::SetAttributes()
{
	assert( IsValid() );
}

void NSIExportLightGroup::SetAttributesAtTime( double, bool )
{
}

void NSIExportLightGroup::Connect( const DelegateTable *i_dag_hash )
{
	MObjectArray items;
	NSIExportUtilities::GetLightsFromLightGroup( Object(), false, items );

	NSI::Context nsi( m_nsi );

	for( unsigned int i = 0; i < items.length(); i++ )
	{
		MString nsi_handle = NSIExportDelegate::NSIHandle( items[i], m_live );
		nsi.Connect( nsi_handle.asChar(), "", m_nsi_handle, "objects" );
	}
}
