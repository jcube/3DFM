#ifndef __DL_OPTIONMENUCMD_H__
#define __DL_OPTIONMENUCMD_H__

#include <maya/MPxCommand.h>
#include <maya/MString.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MArgDatabase;
#endif

class DL_optionMenuCmd : public MPxCommand
{

public:
  virtual MStatus       doIt(const MArgList& args);

  virtual bool          isUndoable() const
                        { return false; }

  static void*          creator()
                        { return new DL_optionMenuCmd; }

  static MSyntax        newSyntax();

private:
  
  MStatus               doCreate(const MArgDatabase& arg_data);
  MStatus               doEdit(const MArgDatabase& arg_data);
  MStatus               doQuery(const MArgDatabase& arg_data);
  MStatus               doCreateOrEdit(const MArgDatabase& arg_data);

  void                  createPopupMenu(
                          const MString& control_name, 
                          int button,
                          const MString& postMenuCommand);

  void                  getControlPath(
                          const MString& control_name,
                          const MString& parent_layout_path,
                          bool& path_found,
                          MString& control_path);

  void                  getStringWithEscapedQuotes(
                          const MString& string,
                          MString& string_with_escaped_quotes);

  MString               m_control_parent_path;
  MString               m_control_path;
  MString               m_control_name;

};

#endif
