#ifndef __DLRENDERGLOBALS_H__
#define __DLRENDERGLOBALS_H__

#include <maya/MPxNode.h>

class dlRenderGlobals : public MPxNode
{
public:
  virtual MStatus compute( const MPlug& plug, MDataBlock& data);

  virtual void postConstructor();

  static void* creator();
  static MStatus initialize();

  static MTypeId id;

 private:
 	static MObject m_renderSettings;
};

#endif
