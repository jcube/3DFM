#include "DL_renderState.h"

#include <maya/MGlobal.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MSelectionList.h>

#include "DL_errors.h"
#include "MU_typeIds.h"

DL_renderState&
DL_renderState::getRenderState()
{
  static DL_renderState render_state;

  return render_state;
}

DL_renderState::DL_renderState()
:
  m_ipr(IPR_STOPPED),
  m_faceid(false)
{
  // nothing else needed
}

DL_renderState::~DL_renderState()
{
  // nothing else needed
}

void
DL_renderState::setRenderingState(e_renderingState i_state)
{
	if (m_renderingState != i_state)
	{
		m_renderingState = i_state;

		MString cmd("delightRenderingStateChangedCB ");
		cmd += i_state;

		MGlobal::executeCommandOnIdle(cmd);
	}
}

MStatus
DL_renderState::setRenderNode(const MString& render_node)
{
	if(render_node == MString(""))
	{
		m_renderNode = MObjectHandle();
		return MStatus::kSuccess;
	}

	MSelectionList selList;
	MStatus status = selList.add(render_node);
	RETURN_ON_FAILED_MSTATUS(status);

	MObject depNode;
	status = selList.getDependNode(0, depNode);
	RETURN_ON_FAILED_MSTATUS(status);

	m_renderNode = MObjectHandle(depNode);
	return status;
}

MString
DL_renderState::getRenderNode() const
{
	if(m_renderNode.isValid())
	{
		MFnDependencyNode depNodeFn(m_renderNode.object());
		return depNodeFn.name();
	}

	return MString();
}

// NOTE: unimplemented.. can't copy these objects
// DL_renderState::DL_renderState(const DL_renderState& other)
// DL_renderState::DL_renderState& operator=(const DL_renderState& other);
