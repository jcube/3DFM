#ifndef __NSIExportPfxGeometry_h
#define __NSIExportPfxGeometry_h

#include "NSIExportDelegate.h"
#include "dlVec.h"

#include <vector>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MRenderLine;
#endif

/**
	\sa NSIExportDelegate
*/
class NSIExportPfxGeometry : public NSIExportDelegate
{
public:
	NSIExportPfxGeometry(
		MFnDagNode &i_dag,
		const NSIExportDelegate::Context& i_context )
	:
		NSIExportDelegate( i_dag, i_context ),
		m_last_nvertices(-1),
		m_chunk_number(0),
		m_total_chunks(0),
		m_stabilized(0),
		m_exportedAsCubic(false)
	{
	}

	virtual void Create();
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );

	virtual bool RegisterCallbacks( void );

	/**
		\brief Provide a set object which should be inspected for attributes that
		affect how the geometry is produced.
	*/
	virtual void MemberOfSet(MObject& i_set);

private:
	static void NodeDirtyCB( MObject &, void *);
	void PipelineOnePfxLine( const MRenderLine *i_line, bool i_cubic );
	bool IsCubic()const;

private:
	int m_last_nvertices;
	int m_chunk_number, m_total_chunks;
	int m_stabilized;

	bool m_exportedAsCubic;

	/* Cumulated curves data which have the same nvertices */
	std::vector<dl::V3f> m_P;
	std::vector<float> m_width;
	std::vector<dl::V3f> m_Cs;
	std::vector<dl::V3f> m_Os;

	MObjectHandle m_set; 
};
#endif
