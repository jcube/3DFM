#ifndef __maya_render_view_h
#define __maya_render_view_h

#include <maya/MRenderView.h>

struct ImageInfos;

/*
	This struct describes a Maya Render View event. It is used inside a wait
	queue.
*/
struct Event
{
	enum Type
	{
		e_open,
		e_write_bucket,
		e_close
	};

	Event() {}

	/*
		write
	*/
	Event(
		ImageInfos *i_image,
		RV_PIXEL *i_buffer,
		int i_xmin, int i_xmax,
		int i_ymin, int i_ymax )
	:
		m_buffer(i_buffer),
		m_type(e_write_bucket),
		m_image(i_image),
		m_xmin(i_xmin), m_xmax(i_xmax),
		m_ymin(i_ymin), m_ymax(i_ymax)
	{
	}

	/*
		close/open
	*/
	Event( Type i_type, ImageInfos *i_image )
	:
		m_type(i_type), m_image(i_image)
	{
	}

	/* This will contain one bucket worth of pixels */
	RV_PIXEL *m_buffer;

	Type m_type;
	ImageInfos *m_image;

	int m_xmin, m_xmax;
	int m_ymin, m_ymax;	
};

/* Register the maya_render_view display with 3Delight. */
void RegisterMayaRenderView();
void RefreshRenderView( float, float, void* );
#endif

