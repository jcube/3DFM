#ifndef __DL_HASH_H__
#define __DL_HASH_H__

#include <maya/MPxCommand.h>

class DL_hash : public MPxCommand
{
public:
  virtual MStatus doIt(const MArgList& args);

  virtual bool isUndoable() const { return false; }

  static void* creator() { return new DL_hash; }

  static MSyntax newSyntax();
private:
};

#endif
