#include "dlSky.h"

#include <maya/MFloatVector.h>
#include <maya/MFnNumericAttribute.h>

#include "DL_utils.h"

MObject dlSky::s_intensity;
MObject dlSky::s_turbidity;
MObject dlSky::s_ground_albedo;

MObject dlSky::s_elevation;
MObject dlSky::s_azimuth;

MObject dlSky::s_sky_tint;
MObject dlSky::s_sun_tint;
MObject dlSky::s_sun_size;
MObject dlSky::s_sun_enable;
MObject dlSky::s_ground_enable;

MObject dlSky::s_wavelengthR;
MObject dlSky::s_wavelengthG;
MObject dlSky::s_wavelengthB;
MObject dlSky::s_uvCoord;
MObject dlSky::s_uCoord;
MObject dlSky::s_vCoord;

MObject dlSky::s_outColor;
MObject dlSky::s_outTransparency;

void* dlSky::creator()
{
	return new dlSky();
}

MStatus dlSky::initialize()
{
	MFnNumericAttribute numAttr;

	s_intensity = numAttr.create(
		"intensity",
		"intensity",
		MFnNumericData::kFloat,
		1.0);
	numAttr.setMin(0.0);
	numAttr.setMax(10.0);
	numAttr.setNiceNameOverride( "Intensity" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_intensity );

	s_turbidity = numAttr.create(
		"turbidity",
		"turbidity",
		MFnNumericData::kFloat,
		3.0);
	numAttr.setMin(0.0);
	numAttr.setMax(10.0);
	numAttr.setNiceNameOverride( "Turbidity" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_turbidity );

	s_ground_albedo = numAttr.createColor(
		"ground_albedo",
		"ground_albedo" );
	numAttr.setDefault( 0.1, 0.1, 0.1 );
	Utilities::setNiceNameColor(numAttr, "Ground Color");
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_ground_albedo );

	s_elevation = numAttr.create(
		"elevation",
		"elevation",
		MFnNumericData::kFloat,
		45.0);
	numAttr.setMin(0.0);
	numAttr.setMax(90.0);
	numAttr.setNiceNameOverride( "Elevation" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_elevation );

	s_azimuth = numAttr.create(
		"azimuth",
		"azimuth",
		MFnNumericData::kFloat,
		90.0);
	numAttr.setMin(0.0);
	numAttr.setMax(360.0);
	numAttr.setNiceNameOverride( "Azimuth" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_azimuth );

	s_sky_tint = numAttr.createColor(
		"sky_tint",
		"sky_tint" );
	numAttr.setDefault( 1.f, 1.f, 1.f );
	Utilities::setNiceNameColor(numAttr, "Sky Tint");
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_sky_tint );

	s_sun_tint = numAttr.createColor(
		"sun_tint",
		"sun_tint" );
	numAttr.setDefault( 1.f, 1.f, 1.f );
	Utilities::setNiceNameColor(numAttr, "Sun Tint");
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_sun_tint );

	s_sun_size = numAttr.create(
		"sun_size",
		"sun_size",
		MFnNumericData::kFloat,
		0.51);
	numAttr.setMin(0.0);
	numAttr.setMax(30.0);
	numAttr.setNiceNameOverride( "Sun Size" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_sun_size );

	s_sun_enable = numAttr.create(
		"sun_enable",
		"sun_enable",
		MFnNumericData::kBoolean,
		1);
	numAttr.setNiceNameOverride( "Draw Sun Disk" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute(s_sun_enable);

	s_ground_enable = numAttr.create(
		"ground_enable",
		"ground_enable",
		MFnNumericData::kBoolean,
		0);
	numAttr.setNiceNameOverride( "Draw Ground" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute(s_ground_enable);

	s_wavelengthR = numAttr.create(
		"wavelengthR",
		"wavelengthR",
		MFnNumericData::kFloat,
		615);
	numAttr.setMin(320.0);
	numAttr.setMax(720.0);
//	numAttr.setHidden( true );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_wavelengthR );

	s_wavelengthG = numAttr.create(
		"wavelengthG",
		"wavelengthG",
		MFnNumericData::kFloat,
		545);
	numAttr.setMin(320.0);
	numAttr.setMax(720.0);
//	numAttr.setHidden( true );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_wavelengthG );

	s_wavelengthB = numAttr.create(
		"wavelengthB",
		"wavelengthB",
		MFnNumericData::kFloat,
		450);
	numAttr.setMin(320.0);
	numAttr.setMax(720.0);
//	numAttr.setHidden( true );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_wavelengthB );

	s_uCoord = numAttr.create( "uCoord", "u", MFnNumericData::kFloat, 0.0);
	numAttr.setHidden( true );
	Utilities::makeShaderInputAttribute(numAttr);
	numAttr.setStorable( false );
	addAttribute( s_uCoord );

	s_vCoord = numAttr.create( "vCoord", "v", MFnNumericData::kFloat, 0.0);
	numAttr.setHidden( true );
	Utilities::makeShaderInputAttribute(numAttr);
	numAttr.setStorable( false );
	addAttribute( s_vCoord );

	s_uvCoord = numAttr.create( "uvCoord", "uv", s_uCoord, s_vCoord );
	numAttr.setHidden( true );
	numAttr.setStorable( false );
	addAttribute( s_uvCoord );

	// Output attributes
	s_outColor = numAttr.createColor( "outColor", "oc" );
	Utilities::makeOutputAttribute(numAttr);
	addAttribute( s_outColor );

	s_outTransparency = numAttr.createColor( "outTransparency", "ot" );
	Utilities::makeOutputAttribute(numAttr);
	addAttribute( s_outTransparency );

	attributeAffects( s_intensity, s_outColor );
	attributeAffects( s_turbidity, s_outColor );
	attributeAffects( s_ground_albedo, s_outColor );
	attributeAffects( s_elevation, s_outColor );
	attributeAffects( s_azimuth, s_outColor );
	attributeAffects( s_sky_tint, s_outColor );
	attributeAffects( s_sun_tint, s_outColor );
	attributeAffects( s_sun_size, s_outColor );
	attributeAffects( s_wavelengthR, s_outColor );
	attributeAffects( s_wavelengthG, s_outColor );
	attributeAffects( s_wavelengthB, s_outColor );

	return MStatus::kSuccess;
}

void dlSky::postConstructor()
{
	MFnDependencyNode dep_node(thisMObject());

	/* Set the default node name */
	MString defaultname = typeName() + "#";

	/* Save the digit in the node name */
	if( isdigit( defaultname.asChar()[0] ) )
	{
		defaultname = "_" + defaultname;
	}

	dep_node.setName(defaultname);

	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlSky::compute(
    const MPlug& i_plug,
    MDataBlock& i_block )
{
	if ((i_plug != s_outColor) && (i_plug.parent() != s_outColor))
	{
		return MS::kUnknownParameter;
	}

	// TODO?
	return MS::kSuccess;
}
