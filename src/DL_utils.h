#ifndef __DL_UTILS_H__
#define __DL_UTILS_H__

#include <math.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <maya/MAnimControl.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MDoubleArray;
class MFloatArray;
class MFloatVector;
class MFnDependencyNode;
class MFnAttribute;
class MFnMesh;
class MFnNumericAttribute;
class MObject;
class MString;
class MStringArray;
class MVectorArray;
class MIntArray;
class MColor;
#endif

#ifdef _WIN32
#define snprintf _snprintf
#endif

namespace Utilities
{
  // Attribute creation utilities
  //
  // Set input attribute properties of the given MFnAttribute
  void makeInputAttribute(MFnAttribute& i_attrFn, bool i_keyable = true);

  /*
    Set input attribute properties of the given MFnAttribute. Smae as above,
    with more appropriate settings for shader intput attributes.
  */
  void makeShaderInputAttribute(MFnAttribute& i_attrFn);

  // Set input & output attribute properties of the given MFnAttribute
  void makeInputOutputAttribute(MFnAttribute& i_attrFn);

  // Set output attribute properties of the given MFnAttribute.
  void makeOutputAttribute(MFnAttribute& i_attrFn);

  // Set nice name for the given io_numAttr of type color.
  void setNiceNameColor(MFnNumericAttribute& io_numAttr, const MString& i_name);

  // Set nice name for the given io_numAttr of type point.
  void setNiceNamePoint(MFnNumericAttribute& io_numAttr, const MString& i_name);

  /*
    Returns a version of i_filename where <f> will be replaced with the frame
    number with the least amount of padding that corresponds to an existing
    file.

    If no matching file exists, then <f> is replaced with frame_number, without
    any padding.
  */
  MString expandFToken( const MString& i_filename, float frame_number );

  float CIEluminance( const MFloatVector& i_color );

  /// returns a formatted frame string.
  std::string FrameID( double i_frame );

  // Returns true when a scene file is being read.
  bool isReadingSceneFile();

  /**
    \brief Returns the classification strings of the specified node type as a
    MStringArray.
  */
  void GetTypeClassificationStrings(
    const MString& i_typeName,
    MStringArray& o_classificationStrings );

  /**
    \brief Returns true if the specified class is satisfied in one of the
    classification strings.
  */
  bool HasClass(
    const MString& i_class,
    const MStringArray i_classificationStrings );
}

bool
getSourceNode(
  MObject& source_node,
  const MFnDependencyNode& dst_node_fn,
  const MString& dst_attr);

bool
getReferenceObject(
  const MFnDependencyNode& dep_node_fn,
  MObject& reference_mesh);

bool
getAttrBool(
  const MObject& node,
  const MString& attr_name,
  bool default_value,
  bool& attr_found);

bool
getAttrBool(
  const MObject& node,
  const MString& attr_name,
  bool default_value);

int
getAttrInt(
  const MObject& node,
  const MString& attr_name,
  int default_value,
  bool& attr_found);

int
getAttrInt(
  const MObject& node,
  const MString& attr_name,
  int default_value);

double
getAttrDouble(
  const MObject& node,
  const MString& attr_name,
  double default_value,
  bool& attr_found);

double
getAttrDouble(
  const MObject& node,
  const MString& attr_name,
  double default_value);

MString
getAttrString(
  const MObject& node,
  const MString& attr_name,
  const MString& default_value,
  bool& attr_found);

MString
getAttrString(
  const MObject& node,
  const MString& attr_name,
  const MString& default_value);

bool
getAttrIntArray(
  const MObject& node,
  const MString& attr_name,
  MIntArray& value);

bool
getAttrDoubleArray(
  const MObject& node,
  const MString& attr_name,
  MDoubleArray& value);

bool
getAttrVectorArray(
  const MObject& node,
  const MString& attr_name,
  MVectorArray& value);

bool
getAttrColor(
  const MObject& node,
  const MString& attr_name,
  MColor& value);

bool
getAttrFloat3(
  const MObject& node,
  const MString& attr_name,
  MFloatArray& value);

bool
isAttrNumeric(
  const MObject& node,
  const MString& attr_name);

bool
isAttrString(
  const MObject& node,
  const MString& attr_name);

void
normalizeKnotValues(
  MDoubleArray& knot_vector,
  double min,
  double max);

// the "current" uv set (if any) will NOT be in the list of extra uv sets
void
getAllUVsetNames(
  MString& current_uv_set,
  MStringArray& extra_uv_sets,
  const MFnMesh& mesh_fn);

void
expandEnvVars(char* dest, unsigned dest_len, const char* src, float frame_num);

void
expandEnvVars(
  char* dest,
  unsigned dest_len,
  const char* src,
  bool do_frame_seq,
  float frame_num);

inline double
normalizeValue(double value, double min, double max)
{
  return (value - min)/(max-min);
}

inline float
roundTimeStep( float i_time )
{
  /* We no longer need to round in here as 3Delight does the magic now. Times
     which are close enough together are merged. */
  return i_time;
}

MString
getStringCommandResult(const MString cmd);

MString
getFurFilename(MString i_node_full_path);

MString
getFluidFilename(MString i_node_full_path);

void
SleepMilliseconds(unsigned i_ms);

/* Converts MTime units to FPS */
double TimeUnitToFPS(MTime::Unit i_unit);

/* Checks if the particle system connected to the instancer is cached. */
bool IsPartsysCached(const MObject& i_instancer_node);

// Returns the path to $DELIGHT; on Windows the backslahes it may contain are
// changed to forward slashes.
//
MString getDelightPath();

// Returns a version of i_path containing forward slashes on Windows.
//
MString getFixedPath(MString i_path);

inline static unsigned char TextureFloatToByte( float v )
{
	if( v < 0.0f )
		v = 0.0f;
	if( v > 1.0f )
		v = 1.0f;
	return static_cast<int>( v * 255.0f );
}

#endif
