#ifndef __DL_EXISTSCMD_H__
#define __DL_EXISTSCMD_H__

#include <maya/MPxCommand.h>

class DL_existsCmd : public MPxCommand
{
public:
  virtual MStatus doIt(const MArgList& args);
  
  virtual bool isUndoable() const { return true; }
  
  static void* creator() {return new DL_existsCmd; }
  
  static MSyntax newSyntax();
};

#endif
