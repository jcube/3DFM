#include "DL_optionMenuCmd.h"
#include <maya/MGlobal.h>
#include <maya/MArgDatabase.h>
#include <maya/MSyntax.h>
#include <algorithm>

MSyntax
DL_optionMenuCmd::newSyntax()
{
  //
  // Description:
  //  This method returns an object which specifies the syntax of the command.
  //

  MSyntax syntax;

  syntax.addFlag("-e", "-edit");
  syntax.addFlag("-en", "-enable", MSyntax::kBoolean);
  syntax.addFlag("-h", "-height", MSyntax::kLong);
  syntax.addFlag("-l", "-label", MSyntax::kString);
  syntax.addFlag("-lmb", "-lmbPostMenuCommand", MSyntax::kString);
  syntax.addFlag("-m", "-manage", MSyntax::kBoolean);
  syntax.addFlag("-q", "-query");
  syntax.addFlag("-rmb", "-rmbPostMenuCommand", MSyntax::kString);
  syntax.addFlag("-w", "-width", MSyntax::kLong);

  syntax.setObjectType(MSyntax::kStringObjects);
  syntax.setMinObjects(0);
  syntax.setMaxObjects(1);

  return syntax;
}

MStatus
DL_optionMenuCmd::doIt(const MArgList& args)
{
  //
  // Description:
  //  This method is called when the command is invoked.
  //  This method parses the arguments to the command and performs the
  //  operation.
  //  This method returns MS::kSuccess if the operations of the command were 
  //  successfully performed, or MS::kFailure if not.
  //

  MStatus status;
  MArgDatabase arg_data(syntax(), args, &status);

  if (status == MS::kFailure)
  {
    return MS::kFailure;
  }

  MString current_layout_path;
  MGlobal::executeCommand("setParent -query", current_layout_path);

  MString path_arg;
  MStringArray object_array;

  status = arg_data.getObjects(object_array);

  if (object_array.length() > 0)
  {
    // The user has specified an object argument. This should be a
    // full path to an existing control, the name of a control that exists
    // as a child of the current layout, or the desired name for a new
    // control that this command should create. We will figure out which it
    // is shortly.
    //
    path_arg = object_array[0];
  }

  MString command;
  MStringArray path_arg_segment_array;
  path_arg.split('|', path_arg_segment_array);

  if (arg_data.isFlagSet("-q") || arg_data.isFlagSet("-e"))
  {
    //
    // We are querying or editing, so we need to get the complete path of the
    // control we are querying/editing.
    //

    if (path_arg_segment_array.length() > 1)
    {
      // The user specified a multi-segmented path to the control. This should
      // be the full path of the control. See if that path is the name of 
      // a control.
      //
      int exists;
      command = "control -exists " + path_arg;
      MGlobal::executeCommand(command, exists);

      if (exists)
      {
        // The user specified path is indeed the path of a control.
        //
        m_control_path = path_arg;
        m_control_name =
          path_arg_segment_array[path_arg_segment_array.length() - 1];
      }
      else
      {
        // The user specified path is not the path of a control.
        //
        MString error;
        error = "No delightOptionMenu named ";
        error += path_arg;
        error += " exists.";
        displayError(error);
        return MS::kFailure;
      }
    }
    else if (path_arg_segment_array.length() == 1)
    {
      // The user specified a single-segment path. They probably
      // specified just the name of the control, which we should be able to
      // find somewhere under the current layout.
      //
      bool path_found;
      getControlPath(path_arg, current_layout_path, path_found, m_control_path);

      if (path_found)
      {
        m_control_name = path_arg;

        //
        // By the time findControl() returned, m_control_path contained 
        // the path to the control.
        //
      }
      else
      {
        // The control did not exist anywhere under the current layout.
        //
        MString error;
        error = "No delightOptionMenu named ";
        error += path_arg;
        error += " exists.";
        displayError(error);
        return MS::kFailure;
      }
    }
    else
    {
      // The user did not specify the name of the control to edit/query.
      //
      MString error;
      error = "No delightOptionMenu control was specified for ";

      if (arg_data.isFlagSet("-q")) 
      {
        error += "query.";
      }
      else
      {
        error += "edit.";
      }

      displayError(error);
      return MS::kFailure;
    }
  }
  else
  {
    //
    // The command has been invoked to create a new control, not to query or
    // edit an existing one.
    //

    if (path_arg_segment_array.length() > 1)
    {
      // The user has specified a multi-segment path name as the name of the
      // control to be specified. This is not allowed.
      //
      MString error;
      error = "Controls can only be created in the current layout. ";
      error += "Can't create a delightOptionMenu named ";
      error += m_control_path;
      error += ".";
      displayError(error);
      return MS::kFailure;
    }
    else if (path_arg_segment_array.length() == 1)
    {
      // The user has specified a name for the new control.
      // We will assemble the control path out of the current layout path and
      // the user specified name.
      //
      m_control_name = path_arg_segment_array[0];
      m_control_path = current_layout_path + "|" + m_control_name;

      int exists;
      command = "control -query -exists " + m_control_path;
      MGlobal::executeCommand(command, exists);

      if (exists == 1)
      {
        MString error;
        error = "A control named ";
        error += m_control_name;
        error += " already exists in the current layout. ";
        error += "Could not create a delightOptionMenu named ";
        error += m_control_name;
        error += ".";
        displayError(error);
        return MS::kFailure;
      }
    }
    else
    {
      //
      // The user has not specified a name for the new control, so we will
      // create one based on the type of control we are creating. We will add
      // a number to the end of it, and ensure that no other control by that
      // name already exists.
      //

      int exists;
      unsigned number = 1;

      for (;;)
      {
        m_control_name = "delightOptionMenu";
        m_control_name += number;
        m_control_path = current_layout_path + "|" + m_control_name;

        command = "control -query -exists " + m_control_path;
        MGlobal::executeCommand(command, exists);

        if (exists == 0)
        {
          // We have constructed a control name that is not already in use.
          // 
          break;
        }
        
        number++;
      }
    }
  }


  if (status == MS::kSuccess)
  {
    if (arg_data.isFlagSet("-q"))
    {
      status = doQuery(arg_data);
    }
    else if (arg_data.isFlagSet("-e"))
    {
      status = doEdit(arg_data);
    }
    else
    {
      status = doCreate(arg_data);
    }
  }

  return status;
}

MStatus
DL_optionMenuCmd::doCreate(const MArgDatabase& arg_data)
{
  //
  // Description:
  //  This method is called when the command has been invoked to create a new
  //  control.
  //  This method creates the new control. 
  //  To the caller, this command should behave much like any other command
  //  that creates a control. In actual fact, this command creates several
  //  layouts and controls that, combined together, just look as if they are 
  //  one control.
  //  This method returns MS::kSuccess if the control was successfully created,
  //  or MS::kFailure if not.
  //

  //
  // Determine which command flags were used, and what the arguments to them
  // were. At this point we are only looking at the flags that are allowed on
  // creation only.
  //

  bool width_flag_used = false;
  bool height_flag_used = false;

  int width;
  int height;

  if (arg_data.isFlagSet("-w"))
  {
    width_flag_used = true;
    arg_data.getFlagArgument("-w", 0, width);
  }

  if (arg_data.isFlagSet("-h"))
  {
    height_flag_used = true;
    arg_data.getFlagArgument("-h", 0, height);
  }

  //
  // Invoke other MEL commands to create the layouts and controls that make up
  // this so-called control.
  //

  MString frame_layout_name;
  MString form_layout_name;
  MString text_btn_name;
  MString icon_btn_name;

  MString command;
  command = "frameLayout -labelVisible false ";

  // We should probably discard the frame layout in 2016 altogether. It accepts
  // -broderVisible true but draws a white (ish) border which does not look
  // good. Discarding the frame layout for 2016 means fixes elsewhere in this
  // file to deal with the different gadget / layout hierarchy.
  //
#if MAYA_API_VERSION < 201600
  command += "-borderStyle \"out\" ";
#endif

  if (height_flag_used)
  {
    command += "-height ";
    command += height;
    command += " ";
  }

  command += m_control_name;
  MGlobal::executeCommand(command, frame_layout_name);

  command = "formLayout ";
  command += m_control_name;
  command += "_form";
  MGlobal::executeCommand(command, form_layout_name);


  command = "iconTextButton -style \"textOnly\" ";

  if (height_flag_used)
  {
    command += "-height ";
    command += height;
    command += " ";
  }

  if (width_flag_used)
  {
    command += "-width ";
    command += std::max(width - 20, 0);
    command += " ";
  }

#if MAYA_API_VERSION >= 201600
  command += " -enableBackground true -backgroundColor 0.365 0.365 0.365 ";
#endif

  command += m_control_name;
  command += "_text_btn";
  MGlobal::executeCommand(command, text_btn_name);


  command = "iconTextButton -style \"iconOnly\" ";

  if (height_flag_used)
  {
    command += "-height ";
    command += height;
    command += " ";
  }

  command += "-width 20 ";

#if MAYA_API_VERSION < 201600
  command += "-image \"shelfOptions.xpm\" ";
#else
  command += "-image \"optionMenuArrow.png\" ";
  command += "-enableBackground true -backgroundColor 0.365 0.365 0.365 ";
#endif

  command += m_control_name;
  command += "_icon_btn";
  MGlobal::executeCommand(command, icon_btn_name);
  

  command = "formLayout -edit ";
  command += 
    "-attachForm " + text_btn_name + "\"left\" 0 ";
  command += 
    "-attachForm " + text_btn_name + "\"top\" 0 ";
  command += 
    "-attachControl " + text_btn_name + "\"right\" 0 " + icon_btn_name + " ";
  command += 
    "-attachForm " + text_btn_name + "\"bottom\" 0 ";
  command += 
    "-attachNone " + icon_btn_name + "\"left\" ";
  command += 
    "-attachForm " + icon_btn_name + "\"top\" 0 ";
  command += 
    "-attachForm " + icon_btn_name + "\"right\" 0 ";
  command += 
    "-attachForm " + icon_btn_name + "\"bottom\" 0 ";
  command += form_layout_name;
  MGlobal::executeCommand(command);

  command = "setParent ..";
  MGlobal::executeCommand(command); // setParent from form
  MGlobal::executeCommand(command); // setParent from control

  // When this command is invoked to create a control, the return value of the
  // command is the name of the control that was created. Set the return value
  // now. 
  //
  setResult(m_control_name);

  // Perform operations that are performed the same way whether the command
  // has been invoked to create a new control or to edit an existing one.
  //
  return doCreateOrEdit(arg_data);
}

MStatus
DL_optionMenuCmd::doEdit(const MArgDatabase& arg_data)
{
  //
  // Description:
  //  This method is called when the command has been invoked to edit an
  //  existing control.
  //  This method returns MS::kSuccess if the control was successfully edited,
  //  or MS::kFailure if not.
  //

  if (arg_data.isFlagSet("-w"))
  {
      MString error;
      error = "The -w/width flag cannot be used in edit. ";
      error += "The width can only be set at control creation time.";
      displayError(error);
      return MS::kFailure;
  }

  if (arg_data.isFlagSet("-h"))
  {
      MString error;
      error = "The -h/height flag cannot be used in edit. ";
      error += "The height can only be set at control creation time.";
      displayError(error);
      return MS::kFailure;
  }

  // Perform operations that are performed the same way whether the command
  // has been invoked to create a new control or to edit an existing one.
  //
  return doCreateOrEdit(arg_data);
}

MStatus
DL_optionMenuCmd::doCreateOrEdit(const MArgDatabase& arg_data)
{
  //
  // Description:
  //  This method is called when the command has been invoked to create or 
  //  edit a control. This method does the things that require the same code
  //  whether the control existed prior to this invocation of the command 
  //  or not. 
  //  This method returns MS::kSuccess if the control was successfully edited,
  //  or MS::kFailure if not.
  //

  MStatus status = MS::kSuccess;
  
  // At this point the control should exist.
  //
  MString text_btn_path = 
    m_control_path 
      + "|" + m_control_name + "_form"
      + "|" + m_control_name + "_text_btn";
  
  MString command = "iconTextButton -q -exists " + text_btn_path;
  int gadget_exists = 0;
  MGlobal::executeCommand(command, gadget_exists);
  
  if (!gadget_exists)
    return MStatus::kFailure;
    
  //
  // Determine which command flags were used, and what the arguments to them
  // were.
  //

  bool enable_flag_used = false;
  bool manage_flag_used = false;
  bool label_flag_used = false;
  bool lmb_post_menu_command_flag_used = false;
  bool rmb_post_menu_command_flag_used = false;

  bool enable;
  bool manage;
  MString label;
  MString lmb_post_menu_command;
  MString rmb_post_menu_command;

  if (arg_data.isFlagSet("-en"))
  {
    enable_flag_used = true;
    arg_data.getFlagArgument("-en", 0, enable);
  }

  if (arg_data.isFlagSet("-m"))
  {
    manage_flag_used = true;
    arg_data.getFlagArgument("-m", 0, manage);
  }

  if (arg_data.isFlagSet("-l"))
  {
    label_flag_used = true;
    arg_data.getFlagArgument("-l", 0, label);
  }

  if (arg_data.isFlagSet("-lmb"))
  {
    lmb_post_menu_command_flag_used = true;
    arg_data.getFlagArgument("-lmb", 0, lmb_post_menu_command);
  }

  if (arg_data.isFlagSet("-rmb"))
  {
    rmb_post_menu_command_flag_used = true;
    arg_data.getFlagArgument("-rmb", 0, rmb_post_menu_command);
  }

  //
  // Make changes to the controls that make up the delightOptionMenu, based on
  // the command flags that have been used.
  //
  MString icon_btn_path =
    m_control_path
      + "|" + m_control_name + "_form"
      + "|" + m_control_name + "_icon_btn";

  if ( (lmb_post_menu_command_flag_used)
    || (rmb_post_menu_command_flag_used))
  {
    if (lmb_post_menu_command_flag_used)
    {
      createPopupMenu(text_btn_path, 1, lmb_post_menu_command);
      createPopupMenu(icon_btn_path, 1, lmb_post_menu_command);
    }

    if (rmb_post_menu_command_flag_used)
    {
      createPopupMenu(text_btn_path, 3, rmb_post_menu_command);
      createPopupMenu(icon_btn_path, 3, rmb_post_menu_command);
    }
  }
  
  if (manage_flag_used)
  {
    command = "control -edit -manage ";
    command += manage;
    command += " ";
    command += m_control_path;
    MGlobal::executeCommand(command);
  }

  if (enable_flag_used)
  {
    command = "iconTextButton -edit -enable ";
    command += enable;
    command += " ";
    command += text_btn_path;
    MGlobal::executeCommand(command);

    command = "iconTextButton -edit -enable ";
    command += enable;
    command += " ";
    command += icon_btn_path;
    MGlobal::executeCommand(command);
  }

  if (label_flag_used)
  {
    command = "iconTextButton -edit -label \"";
    command += label;
    command += "\" ";
    command += text_btn_path;
    MGlobal::executeCommand(command);
  }

  return status;
}

MStatus
DL_optionMenuCmd::doQuery(const MArgDatabase& /*arg_data*/)
{
  //
  // Description:
  //  This method is called when the command has been invoked to query an
  //  existing control.
  //  This method returns MS::kSuccess if the control was successfully queried,
  //  or MS::kFailure if not.
  //

  MStatus status = MS::kSuccess;

  // TODO: Implement this.

  return status;
}

void
DL_optionMenuCmd::createPopupMenu(
  const MString& control_name, 
  int button,
  const MString& post_menu_command)
{
  //
  // Description:
  //  This method creates a new popup menu bound to the specified mouse button 
  //  on the specified control only if non already exist. The specified post 
  //  menu command will be executed each time the popup menu is opened.
  //  Note that the full path of the menu is appended as an argument to the 
  //  post menu command. 
  //

  // Figure if a popup menu already exists
  //
  MString popup_menu_name;
  MStringArray popup_menu_array;  
  MString command = "control -query -popupMenuArray " + control_name;
  MGlobal::executeCommand(command, popup_menu_array);

  for (unsigned i = 0; i < popup_menu_array.length(); ++i)
  {
    command = "popupMenu -query -button " + popup_menu_array[i];
    int curr_button = 0;
    MGlobal::executeCommand(command, curr_button);

    if (curr_button == button)
    { 
      popup_menu_name = popup_menu_array[i];
      break;
    }
  }

  if (popup_menu_name == MString(""))
  {
    command = "popupMenu -button "; 
    command += button; 
    command += " -parent ";
    command += control_name;
    MGlobal::executeCommand(command, popup_menu_name);
  }
  else
  {
    command = "popupMenu -edit -deleteAllItems " + popup_menu_name;
    MGlobal::executeCommand(command);
  }  

  MString post_menu_command_with_escaped_quotes;
  getStringWithEscapedQuotes(
    post_menu_command, 
    post_menu_command_with_escaped_quotes);

  command = "menu -edit -postMenuCommand ";
  command += "\"";
  command += post_menu_command_with_escaped_quotes;
  command += " ";
  command += popup_menu_name;
  command += "\" ";
  command += popup_menu_name;
  MGlobal::executeCommand(command);
}

void
DL_optionMenuCmd::getStringWithEscapedQuotes(
  const MString& string,
  MString& string_with_escaped_quotes)
{
  //
  // Description:
  //  This method takes the specified original string, replaces any quotation
  //  marks in it with backslash-escaped quotes, and stuffs the resulting
  //  string into string_with_escaped_quotes.
  //
  const char* src_string = string.asChar();
  char* dst_string = new char[2 * string.length() + 1];
  unsigned dst_index = 0;
  
  for(unsigned i = 0; i < string.length(); i++)
  {
    if(src_string[i] == '"')
    {
      dst_string[dst_index++] = '\\';
      dst_string[dst_index++] = '"';
    }
    else if(src_string[i] == '\\')
    {
      dst_string[dst_index++] = '\\';
      dst_string[dst_index++] = '\\';
    }
    else
      dst_string[dst_index++] = src_string[i];
  }

  dst_string[dst_index] = '\0';
  string_with_escaped_quotes = MString(dst_string);

  delete[] dst_string;
}

void
DL_optionMenuCmd::getControlPath(
  const MString& control_name,
  const MString& parent_layout_path,
  bool& path_found,
  MString& control_path)
{
  //
  // Description:
  //  This method is called from doIt() when the command has been invoked to
  //  edit or query an existing control. 
  //  This method searches the specified parent layout and any layouts under
  //  it to find a control with the specified name. 
  //  If the control is found, path_found is stuffed with true, and
  //  control_path is stuffed with the full path of the control.
  //

  MString command;
  path_found = false;

  int layout_exists;
  command = "layout -exists " + parent_layout_path;
  MGlobal::executeCommand(command, layout_exists);

  if (layout_exists)
  {
    MStringArray child_array;
    command = "layout -query -childArray " + parent_layout_path;
    MGlobal::executeCommand(command, child_array);

    MString child_name;
    MString child_path;

    for (unsigned i = 0; i < child_array.length(); i++)
    {
      child_name = child_array[i];
      child_path = parent_layout_path + "|" + child_name;

      if (child_name == control_name)
      {
        path_found = true;
        control_path = child_path;
        return;
      }
      else 
      {
        getControlPath(control_name, child_path, path_found, control_path);

        if (path_found)
        {
          return;
        }
      }
    }
  }
}
