/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2013                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include <maya/MDGModifier.h>
#include <maya/MFloatVector.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MGlobal.h>
#include <maya/MMessage.h>
#include <maya/MNodeMessage.h>
#include <maya/MPlugArray.h>

#include <assert.h>

#include "DL_genericShadingNode.h"

#define MAKE_INPUT(attr) \
	CHECK_MSTATUS ( attr.setKeyable(true) ); \
	CHECK_MSTATUS ( attr.setStorable(true) ); \
	CHECK_MSTATUS ( attr.setReadable(true) ); \
	CHECK_MSTATUS ( attr.setWritable(true) );

#define MAKE_OUTPUT(attr) \
	CHECK_MSTATUS ( attr.setKeyable(false) ); \
	CHECK_MSTATUS ( attr.setStorable(false) ); \
	CHECK_MSTATUS ( attr.setReadable(true) ); \
	CHECK_MSTATUS ( attr.setWritable(false) );

MObject DL_genericShadingNode::s_color;
MObject DL_genericShadingNode::s_outColor;
MObject DL_genericShadingNode::s_outTransparency;

void*
DL_genericShadingNode::creator()
{
  DL_genericShadingNode*  new_node;

  new_node = new DL_genericShadingNode();

  return new_node;
}

MStatus
DL_genericShadingNode::initialize()
{
  MFnNumericAttribute nAttr;

  s_color = nAttr.createColor( "color", "c" );
  MAKE_INPUT( nAttr );
  CHECK_MSTATUS( nAttr.setDefault(0.5f, 0.5f, 0.5f) );

  /* output color */
  s_outColor = nAttr.createColor( "outColor", "oc" );
  MAKE_OUTPUT( nAttr );

  /* output transparency */
  s_outTransparency = nAttr.createColor( "outTransparency", "ot" );
  MAKE_OUTPUT( nAttr );

  CHECK_MSTATUS( addAttribute( s_color ) );
  CHECK_MSTATUS( addAttribute( s_outColor ) );
  CHECK_MSTATUS( addAttribute( s_outTransparency ) );

  CHECK_MSTATUS ( attributeAffects( s_color, s_outColor ));

  return MS::kSuccess;
}

void DL_genericShadingNode::postConstructor()
{
  MFnDependencyNode dep_node(thisMObject());

  /* Set the default node name */
  MString defaultname = typeName() + "#";

  /* Save the digit in the node name */
  if( isdigit( defaultname.asChar()[0] ) )
  {
    defaultname = "_" + defaultname;
  }

  dep_node.setName(defaultname);

#if MAYA_API_VERSION <= 201200
  /* We are unable to call initialize MEL proc because the node is not
   * initialized by maya proprly but we use "#" in the default node name,
   * it means the name will be changed as soon as the node will be completely
   * initialized. The DL_genericShadingNode::nameChanged() will immediately
   * remove this callback 
   * With 2013 and up, there is a callback we can use to tell Maya a creation
   * procedure that is called when a node is created through the Hypershade.
   * This is slightly preferable as it allows us to call the node init procedure
   * only in this context, and not when opening a scene.
   */
  MObject mobj = thisMObject();
  MNodeMessage::addNameChangedCallback( mobj, nameChanged, (void*)this );
#endif

  setExistWithoutInConnections(true);
  setExistWithoutOutConnections(true);
}

MStatus DL_genericShadingNode::compute(
    const MPlug& i_plug,
    MDataBlock& i_block )
{
  if ((i_plug != s_outColor) && (i_plug.parent() != s_outColor))
  {
    return MS::kUnknownParameter;
  }

  /* Just transfer s_color to s_outColor */
  MFloatVector& color = i_block.inputValue( s_color ).asFloatVector();

  /* set ouput color attribute */
  MDataHandle outColorHandle = i_block.outputValue( s_outColor );
  MFloatVector& outColor = outColorHandle.asFloatVector();
  outColor = color;
  outColorHandle.setClean();

  return MS::kSuccess;
}

void DL_genericShadingNode::nameChanged(
    MObject &, const MString &i_name, void *i_data)
{
  /* We need this function only once, so immediately remove current callback */
  MMessage::removeCallback( MMessage::currentCallbackId() );

  /* Current node */
  DL_genericShadingNode* node = reinterpret_cast<DL_genericShadingNode*>( i_data );
  assert( i_data );

  /* Call initialize MEL proc */
  MString cmd;
  cmd = "DLM_" + node->typeName() + "_initialize( \"" + i_name + "\" );";
  MGlobal::executeCommand(cmd);
}

