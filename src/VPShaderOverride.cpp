/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#include "VPShaderOverride.h"

#include <maya/MFnDependencyNode.h>

#include "VPFragmentRegistry.h"

MHWRender::MPxShadingNodeOverride* VPShaderOverride::creator(
	const MObject& i_obj )
{
	return new VPShaderOverride( i_obj );
}

VPShaderOverride::VPShaderOverride( const MObject& i_obj ):
	MPxShadingNodeOverride( i_obj )
{
	MString typeName = MFnDependencyNode( i_obj ).typeName();

	MStringArray fragments;
	VPFragmentRegistry::Instance()->Add( typeName, fragments );

	if( fragments.length() > 0 )
	{
		m_fragmentName = fragments[0];
	}
}

VPShaderOverride::~VPShaderOverride()
{
}

MHWRender::DrawAPI VPShaderOverride::supportedDrawAPIs() const
{
	return MHWRender::kOpenGL |
		MHWRender::kDirectX11 |
		MHWRender::kOpenGLCoreProfile;
}

MString VPShaderOverride::fragmentName() const
{
	return m_fragmentName;
}

