#include <maya/MStringArray.h>
#include <maya/MArgDatabase.h>
#include <maya/MSyntax.h>
#include <maya/MArgList.h>

#include <set>
#include <algorithm>
#include <iterator>

#include "DL_mstringSTLutils.h"

#include "DL_stringArrayCmd.h"

static std::multiset<MString, delight_mstringLessThan> g_setA;
static std::multiset<MString, delight_mstringLessThan> g_setB;

MSyntax
DL_stringArrayCmd::newSyntax()
{
  MSyntax syntax;

  // NOTE: These is a dummy syntax so 'help' returns something. It also helps
  // provide half-decent error messages (see the end of doIt()).
  syntax.addFlag("-rd", "-removeDuplicates", MSyntax::kString);
  syntax.addFlag("-i", "-intersect", MSyntax::kString, MSyntax::kString);
  syntax.addFlag("-d", "-difference", MSyntax::kString, MSyntax::kString);

  return syntax;
}

static bool
mstringLessThan(const MString& s1, const MString& s2)
{
  return strcmp(s1.asChar(), s2.asChar()) < 0;
}

MStatus
DL_stringArrayCmd::doIt(const MArgList& args)
{
  typedef std::set<MString, delight_mstringLessThan> tMStringSet;
  typedef std::multiset<MString, delight_mstringLessThan> tMStringMultiSet;

  MString flag1;
  MStringArray arr, arr2;
  unsigned one = 1, two = 2;

  MStatus status;

  if( MS::kSuccess == (status = args.get(0, flag1)) )
  {
    status = MS::kFailure;

    if( (flag1 == "-rd" || flag1 == "-removeDuplicates") &&
        MS::kSuccess == (status = args.get(one, arr)) )
    {
      tMStringSet output_set;
      MStringArray output_array;

      unsigned nStrings = arr.length();
      for( unsigned i = 0; i < nStrings; ++i )
      {
        const MString& s = arr[i];

        if( output_set.insert(s).second )
          output_array.append(s);
      }

      setResult(output_array);
    }

    if( (flag1 == "-i" || flag1 == "-intersect") &&
        MS::kSuccess == (status = args.get(one, arr)) &&
        MS::kSuccess == (status = args.get(two, arr2)) )
    {
      tMStringMultiSet arg1, arg2, intersection;

      for( unsigned i = 0; i < arr.length(); ++i )
        arg1.insert(arr[i]);

      for( unsigned i = 0; i < arr2.length(); ++i )
        arg2.insert(arr2[i]);

      std::set_intersection(
        arg1.begin(), arg1.end(),
        arg2.begin(), arg2.end(),
        std::inserter(intersection, intersection.end()),
        mstringLessThan);

      MStringArray output_array;

      for( tMStringMultiSet::iterator it = intersection.begin();
           it != intersection.end();
           ++it )
      {
        output_array.append(*it);
      }

      setResult(output_array);
    }

    if( (flag1 == "-d" || flag1 == "-difference") &&
        MS::kSuccess == (status = args.get(one, arr)) &&
        MS::kSuccess == (status = args.get(two, arr2)) )
    {
      tMStringMultiSet arg1, arg2, intersection;
      
      for( unsigned i = 0; i < arr.length(); ++i )
        arg1.insert(arr[i]);
      
      for( unsigned i = 0; i < arr2.length(); ++i )
        arg2.insert(arr2[i]);
      
      std::set_difference(
                          arg1.begin(), arg1.end(),
                          arg2.begin(), arg2.end(),
                          std::inserter(intersection, intersection.end()),
                          mstringLessThan);
      
      MStringArray output_array;
      
      for( tMStringMultiSet::iterator it = intersection.begin();
           it != intersection.end();
           ++it )
      {
        output_array.append(*it);
      }
      
      setResult(output_array);
    }
  }

  if( status != MS::kSuccess )
  {
    // This is so we get an error message with line number.
    MArgDatabase arg_data(syntax(), args, &status);
  }

  return status;
}
