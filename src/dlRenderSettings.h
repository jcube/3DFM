/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#ifndef __DLRENDERSETTINGS_H__
#define __DLRENDERSETTINGS_H__

#include <maya/MPxNode.h>

class dlRenderSettings : public MPxNode
{
public:
  virtual MStatus compute(const MPlug& plug, MDataBlock& data);
  virtual void postConstructor();
  static void* creator();
  static MStatus initialize();

public:
  static MTypeId id;
};

#endif
