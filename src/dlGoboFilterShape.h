/*
  Copyright (c) 2018 The 3Delight Team.
*/

#ifndef __dlGoboFilterShape_h__
#define __dlGoboFilterShape_h__

#include <maya/MPxSurfaceShape.h>

class dlGoboFilterShape : public MPxSurfaceShape
{
  public:
    dlGoboFilterShape();
    virtual ~dlGoboFilterShape(); 

    virtual void postConstructor();
    virtual MStatus compute( const MPlug&, MDataBlock& );

    static void* creator();
    static MStatus initialize();

    static MTypeId id;

  private:

    // Input attributes
    static MObject s_map;
    static MObject s_density;
    static MObject s_invert;
    static MObject s_useFilterCoordinateSystem;
    static MObject s_scale;
    static MObject s_scaleS;
    static MObject s_scaleT;
    static MObject s_offset;
    static MObject s_offsetS;
    static MObject s_offsetT;
    static MObject s_sWrapMode;
    static MObject s_tWrapMode;
    // Output attribute
    static MObject s_filter_output;
};

#endif
