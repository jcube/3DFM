#ifndef __dlStandard_H__
#define __dlStandard_H__

#include <maya/MPxNode.h>

class dlStandard : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
  virtual MStatus compute( const MPlug& i_plug, MDataBlock& i_dataBlock);

private:
	static MObject s_base_color;
	static MObject s_outColor;
};

#endif
