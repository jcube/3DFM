===========================
Compiling 3Delight for Maya
===========================

Requirements:
=============

- a clone of the 3Delight for Maya repository on gitlab;
- a 3DelightNSI package;
- Maya;
- the Maya devkit.

A clone of the osl_shaders repository is not mandatory but you will not be able to compile the osl shaders of 3Delight for Maya without it.


Build environment prerequisites
===============================

3Delight:
---------
Download and install the 3DelightNSI package. The shell in which you will compile 3Delight for Maya should have the proper environment to run 3Delight (renderdl, oslc, etc.). Refer to the 3DelightNSI installation instructions for details.

Maya:
-----
Any of the supported Maya versions can be installed. One way to select one for compilation is to define the MAYA_LOCATION env var to the maya installation dir.

Maya dev kit:
-------------
https://www.autodesk.com/developer-network/platform-technologies/maya

Choosing a locations for the prerequisites:
-------------------------------------------
Copy the Maya devkit include dir in $MAYA_LOCATION/devkit/

Although the specific locations of these elements can be defined by more environment variables, the makefiles will find them automatically if they are placed as follows:

- if you have the osl_shaders repository, place your clone next to the 3dfm directory.


Windows-specific requirements
=============================

Cygwin
------
https://www.cygwin.com/setup-x86_64.exe

You will need to install cygwin, as the makefiles require GNU make and rely on cygpath. During the installation, once prompted to select packages, adding the make package to the default selection is all that is needed. Once setup has complete, add c:\cygwin64\bin to your PATH.


Microsoft Visual C++
--------------------
At the time of writing, Visual Studio 2019 or newer is recommended.


Compilation shell environment
=============================

As stated above, you will need to have valid paths set in DELIGHT and MAYA_LOCATION environment variables. On Windows, make sure that your MAYA_LOCATION does not contain double quotes.

Set the current directory to the 3dfm directory.

Source the setup script for your shell, then make.


Using the compiled plug-in
==========================

Once the compilation has completed, start Maya from that shell or command prompt; the initialization scripts also define the proper environment variables so that Maya will find & use the plug-in, shaders & script files you just built.

