/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

global proc
VRS_localIlluminationCreateAttr(string $node)
{
  if (! `attributeQuery -node $node -exists "localIllumination"`)
  {
    addAttr -at bool -ln "localIllumination" -dv 0 $node;
  }

  string $rayDepthAttrs[5] =
  {
    "maxDiffuseDepth",
    "maxReflectionDepth",
    "maxRefractionDepth",
    "maxHairDepth"
  };

  for ($rayDepthAttr in $rayDepthAttrs)
  {
    if (! `attributeQuery -node $node -exists $rayDepthAttr`)
    {
      addAttr -at long -ln $rayDepthAttr -dv 3 $node;
    }
  }
}

global proc
VRS_localIlluminationChanged(int $value)
{
  string $node = VRS_getNode();

  if ($value)
  {
    setAttr ($node + ".maxDiffuseDepth") 0;
    setAttr ($node + ".maxReflectionDepth") 0;
    setAttr ($node + ".maxRefractionDepth") 0;
    setAttr ($node + ".maxHairDepth") 0;
  }
  else
  {
    setAttr ($node + ".maxDiffuseDepth") 2;
    setAttr ($node + ".maxReflectionDepth") 2;
    setAttr ($node + ".maxRefractionDepth") 4;
    setAttr ($node + ".maxHairDepth") 5;
  }

  setAttr ($node + ".localIllumination") $value;
}

global proc
VRS_environmentCreateAttr(string $node)
{
  if (! `attributeQuery -node $node -exists "environment"`)
  {
    addAttr -at message -ln "environment" $node;
  }
}

global proc
VRS_selectAovCreateAttr(string $node)
{
  if (! `attributeQuery -node $node -exists "selectAov"`)
  {
    addAttr -dt "string" -ln "selectAov" $node;
    string $aov = DOV_defaultAov();
    setAttr -type "string" ($node + ".selectAov") $aov;
  }
}

global proc
VRS_selectAovChanged(string $label)
{
  string $node = VRS_getNode();

  // Unsafe : get option menu state using label instead of AOV description
  string $aovs[] = DOV_allAovDescriptions();
  for($aov in $aovs)
  {
    if($label == DOV_uiNameField($aov))
    {
      setAttr -type "string" ($node + ".selectAov") $aov;
      return;
    }
  }
}

global proc
VRS_lightsToRenderCreateAttr(string $node)
{
  if (! `attributeQuery -node $node -exists "lightsToRender"`)
  {
    addAttr -at message -ln "lightsToRender" $node;
  }
}

global proc
VRS_exportOperationCreateAttr(string $node)
{
  if (! `attributeQuery -node $node -exists "exportOperation"` )
  {
    addAttr
      -longName "exportOperation"
      -attributeType long
      -defaultValue 0
      -storable false
      $node;
  }
}

global proc string
VRS_getNode()
{
  string $node = "";

  string $nodes[] = `ls -type "dlViewportRenderSettings" ":*"`;
  if (size($nodes))
  {
    $node = $nodes[0];
  }
  else
  {
    $node = `createNode -skipSelect dlViewportRenderSettings`;
  }

  /* Local Illumination */
  VRS_localIlluminationCreateAttr($node);

  /* Environment */
  VRS_environmentCreateAttr($node);

  /* Light To Render */
  VRS_lightsToRenderCreateAttr($node);

  /* AOV */
  VRS_selectAovCreateAttr($node);

  /* Internal usage */
  VRS_exportOperationCreateAttr($node);

  return $node;
}

global proc
VRS_createPanel(string $panel)
{
}

global proc
VRS_initPanel(string $panel)
{
}

global proc
VRS_createUi(string $panel)
{
  // Top layout.
  string $scroll_layout =
    `scrollLayout
      -enable 1
      -childResizable true
      "scroll_layout"`;

  // "Render Settings"
  string $rs_layout =
  `frameLayout
    -label "Shading"
    -collapsable 0
    -collapse 0
    "rs_layout"`;

    setUITemplate -pushTemplate attributeEditorTemplate;
    // "Local Illumination"
    string $local_illumination_checkbox =
      `checkBoxGrp
        -label "Local Illumination"
        -numberOfCheckBoxes 1
        -onCommand1 "VRS_localIlluminationChanged(1)"
        -offCommand1 "VRS_localIlluminationChanged(0)"
        "local_illumination_checkbox"`;

    // "Environment"
    string $node = VRS_getNode();
    string $plug = $node + ".environment";
    if (objExists($plug))
    {
      AE_environmentNew( $plug );
    }

    // "Lights To Render"
    $plug = $node + ".lightsToRender";
    if (objExists($plug))
    {
      AE_lightsToRenderNew( $plug );
    }

    // "Image Layer"
    string $select_aov_menu =
      `optionMenuGrp
        -label "Image Layer"
        -changeCommand "VRS_selectAovChanged(\"#1\")"
        "select_aov_menu"`;

    string $aovs[] = DOV_allAovDescriptions();
    for($aov in $aovs)
    {
      string $label = DOV_uiNameField($aov);
      menuItem -label $label;
    }

    setUITemplate -popTemplate;
}

global proc
VRS_replaceUi(string $panel)
{
  string $scroll_layout = $panel + "|scroll_layout";
  string $scroll_layout_children[] = `scrollLayout -q -childArray $scroll_layout`;

  string $rs_layout = $scroll_layout_children[0];

  //
  string $node = VRS_getNode();

  // "Local Illumination"
  $plug = $node + ".localIllumination";
  if (objExists($plug))
  {
    string $checkbox = $rs_layout + "|local_illumination_checkbox";

    int $value = `getAttr $plug`;
    checkBoxGrp -e -value1 $value $checkbox;
  }

  // "Environment"
  $plug = $node + ".environment";
  if (objExists($plug))
  {
    AE_environmentReplace($plug);
  }

  // "Light"
  $plug = $node + ".lightsToRender";
  if (objExists($plug))
  {
    AE_lightsToRenderReplace($plug);
  }

  // "Image Layer"
  $plug = $node + ".selectAov";
  if (objExists($plug))
  {
    string $aov_menu = $rs_layout + "|select_aov_menu";

    string $value = `getAttr $plug`;
    string $label = DOV_uiNameField($value);

    if ($label == "")
    {
      string $default_aov = DOV_defaultAov();
      $label = DOV_aovLabel($default_aov);
      setAttr -type "string" $plug $default_aov;
    }
    // Unsafe : set option menu state using label instead of AOV description
    optionMenuGrp -e -value $label $aov_menu;
  }
}

global proc
VRS_addPanel(string $panel)
{
  VRS_createUi($panel);
  VRS_replaceUi($panel);
}

global proc
VRS_removePanel(string $panel)
{
}

global proc string
VRS_saveStateCallback(string $panel)
{
  string $state = "";
  return $state;
}

global proc
VRS_createPanelType()
{
  if (!`scriptedPanelType -exists dlViewportRenderSettingsPanel`)
  {
    scriptedPanelType
      -createCallback "VRS_createPanel"
      -initCallback "VRS_initPanel"
      -addCallback "VRS_addPanel"
      -removeCallback "VRS_removePanel"
      -saveStateCallback "VRS_saveStateCallback"
      -unique true
      dlViewportRenderSettingsPanel;
  }
}

global proc string
VRS_getPanel()
{
  string $panel = "";
  string $panels[] = `getPanel -scriptType "dlViewportRenderSettingsPanel"`;
  if (size($panels))
  {
    $panel = $panels[0];
  }

  return $panel;
}

global proc
NSIOptionBox()
{
  global string $gMainPane;

  string $panel = VRS_getPanel();
  if ($panel == "")
  {
    VRS_createPanelType();

    $panel = `scriptedPanel
      -unParent
      -menuBarVisible false
      -tearOff
      -label "Viewport Render Settings"
      -type dlViewportRenderSettingsPanel`;
  }
  else if (!`scriptedPanel -query -tearOff $panel`)
  {
    scriptedPanel -edit -menuBarVisible false -tearOff $panel;
  }

  int $width = 760;
  int $height = 400;

  string $window = $panel + "Window";
  if (`windowPref -exists $window`)
  {
    $width = `windowPref -q -width $window`;
    $height = `windowPref -q -height $window`;
  }

  showWindow $window;
}
