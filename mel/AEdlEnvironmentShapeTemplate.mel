/*
  Copyright (c) 2012 soho vfx inc.
  Copyright (c) 2012 The 3Delight Team.
*/

global proc AE_dlEnv_UseBgTexChanged(string $node)
{
  int $dim = getAttr($node + ".use_background_texture") == 0;
  editorTemplate -dimControl $node "background_texture" $dim;
}

global proc AEdlEnvironmentShapeTemplate(string $node_name)
{
  editorTemplate -beginScrollLayout;

  AE_addTopSeparatorWithLabel( "environmentSep", "Environment", 69 );
  editorTemplate -addControl "intensity";
  editorTemplate -addControl "_3delight_light_exposure";
  editorTemplate -addControl "texture";
  editorTemplate
    -annotation ("A multiplier applied to the environment color in shading " +
      " operations (similar to 'color gain').")
    -addControl
    "tint";
  editorTemplate
    -annotation "Enables use of the background texture value as the camera background instead of the same texture as for lighting."
    -addDynamicControl "use_background_texture" "AE_dlEnv_UseBgTexChanged";
  editorTemplate
    -annotation "Texture used as background for the camera."
    -addControl "background_texture";

  AE_addSpacer( 24 );

  editorTemplate -addControl "mapping";
  editorTemplate -addControl "_3delight_arealight_visibility";
  AE_addControl( "_3delight_prelit" );
  AE_addControl( "radius" );

  AE_addSeparatorWithLabel( "environmentContributions", "Contributions", 71 );
  editorTemplate -addControl "diffuse_contribution";
  editorTemplate -addControl "subsurface_contribution";
  editorTemplate -addControl "specular_contribution";
  editorTemplate -addControl "hair_contribution";
  editorTemplate -addControl "volume_contribution";

  AE_addSpacer( 10 );

  AE_addSeparatorWithLabel( "lightLinkingSep", "Light Linking", 71 );

  editorTemplate -callCustom
    "AE_illuminatesByDefaultNew"
    "AE_illuminatesByDefaultReplace"
    "instObjGroups";

  AE_addSpacer( 12 );

  editorTemplate -addExtraControls -extraControlsLabel "";

  editorTemplate -suppress usedBy3dfm;

  editorTemplate -suppress message;
  editorTemplate -suppress caching;
  editorTemplate -suppress isHistoricallyInteresting;
  editorTemplate -suppress nodeState;
  editorTemplate -suppress binMembership;
  editorTemplate -suppress hyperLayout;
  editorTemplate -suppress isCollapsed;
  editorTemplate -suppress blackBox;
  editorTemplate -suppress borderConnections;
  editorTemplate -suppress isHierarchicalConnection;
  editorTemplate -suppress publishedNodeInfo;
  editorTemplate -suppress rmbCommand;
  editorTemplate -suppress templateName;
  editorTemplate -suppress templatePath;
  editorTemplate -suppress viewName;
  editorTemplate -suppress iconName;
  editorTemplate -suppress viewMode;
  editorTemplate -suppress templateVersion;
  editorTemplate -suppress uiTreatment;
  editorTemplate -suppress customTreatment;
  editorTemplate -suppress creator;
  editorTemplate -suppress creationDate;
  editorTemplate -suppress containerType;
  editorTemplate -suppress boundingBox;
  editorTemplate -suppress boundingBoxMin;
  editorTemplate -suppress boundingBoxMinX;
  editorTemplate -suppress boundingBoxMinY;
  editorTemplate -suppress boundingBoxMinZ;
  editorTemplate -suppress boundingBoxMax;
  editorTemplate -suppress boundingBoxMaxX;
  editorTemplate -suppress boundingBoxMaxY;
  editorTemplate -suppress boundingBoxMaxZ;
  editorTemplate -suppress boundingBoxSize;
  editorTemplate -suppress boundingBoxSizeX;
  editorTemplate -suppress boundingBoxSizeY;
  editorTemplate -suppress boundingBoxSizeZ;
  editorTemplate -suppress center;
  editorTemplate -suppress boundingBoxCenterX;
  editorTemplate -suppress boundingBoxCenterY;
  editorTemplate -suppress boundingBoxCenterZ;
  editorTemplate -suppress inverseMatrix;
  editorTemplate -suppress worldMatrix;
  editorTemplate -suppress worldInverseMatrix;
  editorTemplate -suppress parentMatrix;
  editorTemplate -suppress parentInverseMatrix;
  editorTemplate -suppress visibility;
  editorTemplate -suppress intermediateObject;
  editorTemplate -suppress template;
  editorTemplate -suppress ghosting;
  editorTemplate -suppress useObjectColor;
  editorTemplate -suppress objectColor;
  editorTemplate -suppress drawOverride;
  editorTemplate -suppress overrideDisplayType;
  editorTemplate -suppress overrideLevelOfDetail;
  editorTemplate -suppress overrideShading;
  editorTemplate -suppress overrideTexturing;
  editorTemplate -suppress overridePlayback;
  editorTemplate -suppress overrideEnabled;
  editorTemplate -suppress overrideVisibility;
  editorTemplate -suppress overrideColor;
  editorTemplate -suppress lodVisibility;
  editorTemplate -suppress renderInfo;
  editorTemplate -suppress identification;
  editorTemplate -suppress layerRenderable;
  editorTemplate -suppress layerOverrideColor;
  editorTemplate -suppress renderLayerInfo;
  editorTemplate -suppress ghostingControl;
  editorTemplate -suppress ghostCustomSteps;
  editorTemplate -suppress ghostPreSteps;
  editorTemplate -suppress ghostPostSteps;
  editorTemplate -suppress ghostStepSize;
  editorTemplate -suppress ghostFrames;
  editorTemplate -suppress ghostColorPreA;
  editorTemplate -suppress ghostColorPre;
  editorTemplate -suppress ghostColorPreR;
  editorTemplate -suppress ghostColorPreG;
  editorTemplate -suppress ghostColorPreB;
  editorTemplate -suppress ghostColorPostA;
  editorTemplate -suppress ghostColorPost;
  editorTemplate -suppress ghostColorPostR;
  editorTemplate -suppress ghostColorPostG;
  editorTemplate -suppress ghostColorPostB;
  editorTemplate -suppress ghostRangeStart;
  editorTemplate -suppress ghostRangeEnd;
  editorTemplate -suppress ghostDriver;
  editorTemplate -suppress renderType;
  editorTemplate -suppress renderVolume;
  editorTemplate -suppress visibleFraction;
  editorTemplate -suppress motionBlur;
  editorTemplate -suppress visibleInReflections;
  editorTemplate -suppress visibleInRefractions;
  editorTemplate -suppress castsShadows;
  editorTemplate -suppress receiveShadows;
  editorTemplate -suppress maxVisibilitySamplesOverride;
  editorTemplate -suppress maxVisibilitySamples;
  editorTemplate -suppress geometryAntialiasingOverride;
  editorTemplate -suppress antialiasingLevel;
  editorTemplate -suppress shadingSamplesOverride;
  editorTemplate -suppress shadingSamples;
  editorTemplate -suppress maxShadingSamples;
  editorTemplate -suppress volumeSamplesOverride;
  editorTemplate -suppress volumeSamples;
  editorTemplate -suppress depthJitter;
  editorTemplate -suppress ignoreSelfShadowing;
  editorTemplate -suppress primaryVisibility;
  editorTemplate -suppress referenceObject;
  editorTemplate -suppress compInstObjGroups;
  editorTemplate -suppress tweak;
  editorTemplate -suppress relativeTweak;
  editorTemplate -suppress controlPoints;
  editorTemplate -suppress weights;
  editorTemplate -suppress tweakLocation;
  editorTemplate -suppress blindDataNodes;
  editorTemplate -suppress uvPivot;
  editorTemplate -suppress uvPivotX;
  editorTemplate -suppress uvPivotY;
  editorTemplate -suppress uvSet;
  editorTemplate -suppress currentUVSet;
  editorTemplate -suppress displayImmediate;
  editorTemplate -suppress displayColors;
  editorTemplate -suppress displayColorChannel;
  editorTemplate -suppress currentColorSet;
  editorTemplate -suppress colorSet;
  editorTemplate -suppress ignoreHwShader;
  editorTemplate -suppress doubleSided;
  editorTemplate -suppress opposite;
  editorTemplate -suppress smoothShading;
  editorTemplate -suppress boundingBoxScale;
  editorTemplate -suppress boundingBoxScaleX;
  editorTemplate -suppress boundingBoxScaleY;
  editorTemplate -suppress boundingBoxScaleZ;
  editorTemplate -suppress featureDisplacement;
  editorTemplate -suppress initialSampleRate;
  editorTemplate -suppress extraSampleRate;
  editorTemplate -suppress textureThreshold;
  editorTemplate -suppress normalThreshold;
  editorTemplate -suppress displayHWEnvironment;
  editorTemplate -suppress collisionOffsetVelocityIncrement;
  editorTemplate -suppress collisionDepthVelocityIncrement;
  editorTemplate -suppress collisionOffsetVelocityMultiplier;
  editorTemplate -suppress collisionDepthVelocityMultiplier;
  editorTemplate -suppress objectColorRGB;
  editorTemplate -suppress selectionChildHighlighting;
  editorTemplate -suppress hiddenInOutliner;
  editorTemplate -suppress wireColorRGB;
  editorTemplate -suppress frozen;
  editorTemplate -suppress useOutlinerColor;
  editorTemplate -suppress outlinerColor;
  editorTemplate -suppress hardwareFogMultiplier;
  editorTemplate -suppress holdOut;

  editorTemplate -endScrollLayout;
}
