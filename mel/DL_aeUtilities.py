import maya.mel
import delight.shaderquery

def _FindOptionsForAttribute(query, attribute):
    for p in query.params:
        if p.name == attribute or p.FindMetadata('attribute') == attribute:
            return p.FindMetadata('options')
    return None
 
def AddColorSpaceControl(node, file_attribute):
    cs_attribute = file_attribute + '_meta_colorspace'
    # Find shader file.
    shaderfile = maya.mel.eval('dlOslUtils -shaderfilename "' + node + '"')
    choices = 'auto'
    # Find options metadata for this attribute.
    if shaderfile:
        si = delight.shaderquery.GetShaderInfo(shaderfile)
        if si:
            opt = _FindOptionsForAttribute(si, cs_attribute)
            if opt:
                choices = opt
    # Add widget.
    maya.mel.eval(
        'AE_addOptionMenuControlWithLabel("' + cs_attribute +
        '", "' + choices + '", "Color Space")')

# vim: set softtabstop=4 expandtab shiftwidth=4:
