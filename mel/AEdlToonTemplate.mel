/*
   Copyright (c) 2012 soho vfx inc.
   Copyright (c) 2019 The 3Delight Team.
*/

global proc
AE_dlToonColorRampNew(string $curve_plug)
{
  string $list_msg = "Toon Colors";
  text -label $list_msg -align "center" -height 20 -font "smallObliqueLabelFont" -enable false;

  AEmakeRampControlInteractiveNew $curve_plug;
  setParent ..;
}


global proc
AE_dlToonOutlineRampNew(string $curve_plug)
{
  string $list_msg = "Outline Colors";
  text -label $list_msg -align "center" -height 20 -font "smallObliqueLabelFont" -enable false;

  AEmakeRampControlInteractiveNew $curve_plug;
  setParent ..;
}


global proc
AE_dlToonSpecularRampNew(string $curve_plug)
{
  string $list_msg = "Specular Colors";
  text -label $list_msg -align "center" -height 20 -font "smallObliqueLabelFont" -enable false;

  AEmakeRampControlInteractiveNew $curve_plug;
  setParent ..;
}


global proc
AE_dlToonColorRampReplace(string $curve_plug)
{
  AEmakeRampControlInteractiveReplace $curve_plug;
}



global proc
AE_TOON_SilhouetteChanged(string $node)
{
  int $dim = 1-getAttr($node + ".outline_silhouettes_enable");

  editorTemplate -dimControl $node "outline_silhouettes_color" $dim;
  editorTemplate -dimControl $node "outline_silhouettes_opacity" $dim;
  editorTemplate -dimControl $node "outline_silhouettes_width" $dim;
  editorTemplate -dimControl $node "outline_silhouettes_sensitivity" $dim;
  editorTemplate -dimControl $node "outline_silhouettes_tint_color" $dim;
  // Intentionally introducing the typo in "silouhettes" to match the shader
  editorTemplate -dimControl $node "outline_silouhettes_priority" $dim;
}


global proc
AE_TOON_FoldsChanged(string $node)
{
  int $dim = 1-getAttr($node + ".outline_folds_enable");

  editorTemplate -dimControl $node "outline_folds_color" $dim;
  editorTemplate -dimControl $node "outline_folds_opacity" $dim;
  editorTemplate -dimControl $node "outline_folds_width" $dim;
  editorTemplate -dimControl $node "outline_folds_sensitivity" $dim;
  editorTemplate -dimControl $node "outline_folds_tint_color" $dim;
  editorTemplate -dimControl $node "outline_folds_priority" $dim;
}


global proc
AE_TOON_CreasesChanged(string $node)
{
  int $dim = 1-getAttr($node + ".outline_creases_enable");

  editorTemplate -dimControl $node "outline_creases_color" $dim;
  editorTemplate -dimControl $node "outline_creases_opacity" $dim;
  editorTemplate -dimControl $node "outline_creases_width" $dim;
  editorTemplate -dimControl $node "outline_creases_tint_color" $dim;
  editorTemplate -dimControl $node "outline_creases_priority" $dim;
}

global proc
AE_TOON_ObjectsChanged(string $node)
{
  int $dim = 1-getAttr($node + ".outline_objects_enable");

  editorTemplate -dimControl $node "outline_objects_color" $dim;
  editorTemplate -dimControl $node "outline_objects_opacity" $dim;
  editorTemplate -dimControl $node "outline_objects_width" $dim;
  editorTemplate -dimControl $node "outline_objects_tint_color" $dim;
  editorTemplate -dimControl $node "outline_objects_priority" $dim;
}

global proc
AE_TOON_TextureChanged(string $node)
{
  int $dim = 1-getAttr($node + ".outline_texture_enable");

  editorTemplate -dimControl $node "outline_texture_source" $dim;
  editorTemplate -dimControl $node "outline_texture_color" $dim;
  editorTemplate -dimControl $node "outline_texture_opacity" $dim;
  editorTemplate -dimControl $node "outline_texture_width" $dim;
  editorTemplate -dimControl $node "outline_texture_tint_color" $dim;
  editorTemplate -dimControl $node "outline_texture_sensitivity" $dim;
  editorTemplate -dimControl $node "outline_texture_priority" $dim;
}

global proc AEdlToonTemplate(string $node)
{
  editorTemplate -beginScrollLayout;

  editorTemplate -callCustom AEshaderTypeNew AEshaderTypeReplace "message";

  // Colors
  AE_addSeparatorWithLabel( "colorSep", "Colors", 50 );
  editorTemplate -callCustom
    "AE_dlToonColorRampNew"
    "AE_dlToonColorRampReplace"
    "color";

  AE_addControl( "physical_layer" );
  editorTemplate -label "Tint" -addControl "color_tint";
  editorTemplate -label "Weight" -addControl "diffuse_weight";
  editorTemplate -label "Opacity" -addControl "opacity";

  // Specular
  AE_addSeparatorWithLabel( "specularSep", "Specular", 68 );
  editorTemplate -label "Weight" -addControl "specular_weight";
  editorTemplate -label "Color" -addControl "specular_color";
  editorTemplate -label "Roughness" -addControl "specular_roughness";

  // Refraction
  AE_addSeparatorWithLabel( "refractSep", "Refraction", 68 );
  editorTemplate -label "Weight" -addControl "refract_weight";
  editorTemplate -label "Color" -addControl "refract_color";
  editorTemplate -label "IOR" -addControl "refract_ior";

  // Incandescence
  AE_addSeparatorWithLabel( "incanSep", "Incandescence", 68 );
  editorTemplate -label "Color" -addControl "incandescence";
  editorTemplate -label "Intensity" -addControl "incandescence_intensity";

  // Outlines
  AE_addSeparatorWithLabel( "outlineSep", "Outlines", 68 );

  // Fade
  AE_addNavigationControlWithLabel("outline_fade", "Fade");

  AE_addSeparatorWithLabel( "outlineSilSep", "Silhouette", 68 );
  editorTemplate -label "Enable" -addControl "outline_silhouettes_enable"
    AE_TOON_SilhouetteChanged;
  editorTemplate -label "Color" -addControl "outline_silhouettes_color";
  editorTemplate -label "Opacity" -addControl "outline_silhouettes_opacity";
  editorTemplate -label "Width" -addControl "outline_silhouettes_width";
  editorTemplate -label "Sensitivity" -addControl "outline_silhouettes_sensitivity";
  editorTemplate -label "Tint" -addControl "outline_silhouettes_tint_color";
  // Intentionally introducing the typo in "silouhettes" to match the shader
  editorTemplate -label "Priority" -addControl "outline_silouhettes_priority";

  AE_addSeparatorWithLabel( "outlineFoldSep", "Folds", 68 );
  editorTemplate -label "Enable" -addControl "outline_folds_enable"
    AE_TOON_FoldsChanged;
  editorTemplate -label "Color" -addControl "outline_folds_color";
  editorTemplate -label "Opacity" -addControl "outline_folds_opacity";
  editorTemplate -label "Width" -addControl "outline_folds_width";
  editorTemplate -label "Sensitivity" -addControl "outline_folds_sensitivity";
  editorTemplate -label "Tint" -addControl "outline_folds_tint_color";
  editorTemplate -label "Priority" -addControl "outline_folds_priority";

  AE_addSeparatorWithLabel( "outlineCreasesSep", "Creases", 68 );
  editorTemplate -label "Enable" -addControl "outline_creases_enable"
    AE_TOON_CreasesChanged;
  editorTemplate -label "Color" -addControl "outline_creases_color";
  editorTemplate -label "Opacity" -addControl "outline_creases_opacity";
  editorTemplate -label "Width" -addControl "outline_creases_width";
  editorTemplate -label "Tint" -addControl "outline_creases_tint_color";
  editorTemplate -label "Priority" -addControl "outline_creases_priority";

  AE_addSeparatorWithLabel( "outlineObjectsSep", "Objects", 68 );
  editorTemplate -label "Enable" -addControl "outline_objects_enable"
    AE_TOON_ObjectsChanged;
  editorTemplate -label "Color" -addControl "outline_objects_color";
  editorTemplate -label "Opacity" -addControl "outline_objects_opacity";
  editorTemplate -label "Width" -addControl "outline_objects_width";
  editorTemplate -label "Tint" -addControl "outline_objects_tint_color";
  editorTemplate -label "Priority" -addControl "outline_objects_priority";

  AE_addSeparatorWithLabel( "outlineTextureSep", "Texture", 68 );
  editorTemplate -label "Enable" -addControl "outline_texture_enable"
    AE_TOON_TextureChanged;
  editorTemplate -label "Texture" -addControl "outline_texture_source";
  editorTemplate -label "Color" -addControl "outline_texture_color";
  editorTemplate -label "Opacity" -addControl "outline_texture_opacity";
  editorTemplate -label "Width" -addControl "outline_texture_width";
  editorTemplate -label "Sensitivity" -addControl "outline_texture_sensitivity";
  editorTemplate -label "Tint" -addControl "outline_texture_tint_color";
  editorTemplate -label "Priority" -addControl "outline_texture_priority";

  AE_addSeparatorWithLabel( "outlineBackfacesSep", "Backfaces", 68 );
  editorTemplate -label "Draw Outlines on Backfacing Surfaces"
    -addControl "enable_backfacing_outlines";

  // displace / normal map / bump
  AE_addDispNormalBumpGadgets( 1 );

  AE_addGeometryGroup();
  AE_addAOVGroup("aovGroup");

  AE_addSpacer( 12 );

  // Supressing this attributes tagged as "null" 
  editorTemplate -suppress "outline_alpha_enable";
  editorTemplate -suppress "outline_alpha_color";
  editorTemplate -suppress "outline_alpha_tint_color";
  editorTemplate -suppress "outline_alpha_opacity";
  editorTemplate -suppress "outline_alpha_width";
  editorTemplate -suppress "outline_alpha_sensitivity";
  editorTemplate -suppress "outline_alpha_priority";

  editorTemplate -addExtraControls -extraControlsLabel "";
  editorTemplate -suppress caching;
  editorTemplate -suppress nodeState;
  editorTemplate -suppress "uvCoord";

  editorTemplate -suppress "usedBy3dfm";
  editorTemplate -suppress "version";
  editorTemplate -suppress "frozen";

  editorTemplate -endScrollLayout;
}
