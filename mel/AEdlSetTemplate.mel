/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

proc int enableCheckBoxWidth()
{
  return 9;
}

proc string createControlsForBoolAttr(
  string $enable_attr,
  string $bool_attr,
  string $layout)
{
  global int $gAttributeEditorTemplateLabelWidth;
  global int $gAttributeEditorTemplateSingleWidgetWidth;
  global int $gAttributeEditorTemplateExtraWidth;

  int $cb_width = enableCheckBoxWidth();

  $layout = `rowLayout
    -numberOfColumns 3
    -columnWidth 1 $cb_width
    -columnWidth 2 ($gAttributeEditorTemplateLabelWidth - $cb_width - 1)
    -columnWidth 3 (3 * $gAttributeEditorTemplateSingleWidgetWidth +
      $gAttributeEditorTemplateExtraWidth)
    $layout`;

  iconTextCheckBox
    -style "textOnly"
    -label ""
    -bgc 0.17 0.17 0.17
    -width 9
    -height 9
    (plugAttr($enable_attr) + "CheckBox");
  text -label "";

  string $node = plugNode($bool_attr);
  string $attr = plugAttr($bool_attr);
  string $label = `attributeQuery -node $node -niceName $attr`;

  string $cb = `checkBox -label $label (plugAttr($bool_attr) + "CheckBox")`;
  setParent ..;

  return $layout;
}

proc connectControlsForBoolAttr(
  string $enable_attr,
  string $bool_attr,
  string $layout)
{
  // Connect the gadgets to their related attributes
  connectControl (plugAttr($enable_attr) + "CheckBox") $enable_attr;
  connectControl (plugAttr($bool_attr) + "CheckBox") $bool_attr;

  // Add a script job to monitor changes on the $enable_attr; the sensitivity
  // of the other gadgets is dependent on its value.
  //
  string $layout2 = `rowLayout -q -fullPathName $layout`;

  string $cmd = "AE_SET_enableAttrChangedCB \"" + $enable_attr + "\" " +
    "\"" + $layout2 + "\"";

  scriptJob
    -replacePrevious
    -parent (plugAttr($enable_attr) + "CheckBox")
    -attributeChange $enable_attr $cmd;

  // Makle sure the sensitivity reflects the enable_attr current state.
  eval($cmd);
}

proc string createControlsForIntAttr(
  string $enable_attr,
  string $int_attr,
  string $label,
  string $layout )
{
  global int $gAttributeEditorTemplateLabelWidth;
  global int $gAttributeEditorTemplateSingleWidgetWidth;
  global int $gAttributeEditorTemplateExtraWidth;

  int $cb_width = enableCheckBoxWidth();

  $layout = `rowLayout
    -numberOfColumns 4
    -adjustableColumn 4
    -columnWidth 1 $cb_width
    -columnWidth 2 ($gAttributeEditorTemplateLabelWidth - $cb_width - 1)
    -columnWidth 3 $gAttributeEditorTemplateSingleWidgetWidth
    -columnAlign 2 "right"
    $layout`;

  iconTextCheckBox
    -style "textOnly"
    -label ""
    -bgc 0.17 0.17 0.17
    -width 9
    -height 9
    (plugAttr($enable_attr) + "CheckBox");

  string $attr = plugAttr($int_attr);
  if($label == "")
  {
    string $node = plugNode($int_attr);
    $label = `attributeQuery -node $node -niceName $attr`;
  }

  text -label $label;

  intField ($attr + "IntField");

  setParent ..;

  return $layout;
}

proc connectControlsForIntAttr(
  string $enable_attr,
  string $int_attr,
  string $layout)
{
  // Connect the gadgets to their related attributes
  connectControl (plugAttr($enable_attr) + "CheckBox") $enable_attr;
  connectControl (plugAttr($int_attr) + "IntField") $int_attr;

  // Add a script job to monitor changes on the $enable_attr; the sensitivity
  // of the other gadgets is dependent on its value.
  //
  string $layout2 = `rowLayout -q -fullPathName $layout`;

  string $cmd = "AE_SET_enableAttrChangedCB \"" + $enable_attr + "\" " +
    "\"" + $layout2 + "\"";

  scriptJob
    -replacePrevious
    -parent (plugAttr($int_attr) + "IntField")
    -attributeChange $enable_attr $cmd;

  // Makle sure the sensitivity reflects the enable_attr current state.
  eval($cmd);
}

proc string createControlsForEnumAttr(
  string $enable_attr,
  string $enum_attr,
  string $label,
  string $layout )
{
  global int $gAttributeEditorTemplateLabelWidth;
  global int $gAttributeEditorTemplateSingleWidgetWidth;
  global int $gAttributeEditorTemplateExtraWidth;

  int $cb_width = enableCheckBoxWidth();

  $layout = `rowLayout
    -numberOfColumns 4
    -adjustableColumn 4
    -columnWidth 1 $cb_width
    -columnWidth 2 ($gAttributeEditorTemplateLabelWidth - $cb_width - 1)
    -columnWidth 3 (3 * $gAttributeEditorTemplateSingleWidgetWidth +
      $gAttributeEditorTemplateExtraWidth)
    -columnAlign 2 "right"
    $layout`;

  iconTextCheckBox
    -style "textOnly"
    -label ""
    -bgc 0.17 0.17 0.17
    -width 9
    -height 9
    (plugAttr($enable_attr) + "CheckBox");

  string $attr = plugAttr($enum_attr);
  if($label == "")
  {
    string $node = plugNode($enum_attr);
    $label = `attributeQuery -node $node -niceName $attr`;
  }

  text -label $label;

  // FIXME : The fucker doesn't want to be aligned properly!
  attrEnumOptionMenu -attribute $enum_attr ($attr + "OptionMenu");

  setParent ..;

  return $layout;
}

proc connectControlsForEnumAttr(
  string $enable_attr,
  string $enum_attr,
  string $layout)
{
  // Connect the gadgets to their related attributes
  connectControl (plugAttr($enable_attr) + "CheckBox") $enable_attr;
  attrEnumOptionMenu
    -edit
    -attribute $enum_attr
    (plugAttr($enum_attr) + "OptionMenu");

  // Add a script job to monitor changes on the $enable_attr; the sensitivity
  // of the other gadgets is dependent on its value.
  string $layout2 = `rowLayout -q -fullPathName $layout`;

  string $cmd = "AE_SET_enableAttrChangedCB \"" + $enable_attr + "\" " +
    "\"" + $layout2 + "\"";

  scriptJob
    -replacePrevious
    -parent (plugAttr($enum_attr) + "OptionMenu")
    -attributeChange $enable_attr $cmd;

  // Make sure the sensitivity reflects the enable_attr current state.
  eval($cmd);
}

global proc AE_SET_enableAttrChangedCB(string $enable_attr, string $layout)
{
  // Assuming that the first child is the enable_attr control, adjust
  // sensitivity of all the other children layout according to the value of
  // enable_attr.
  //
  int $value = getAttr($enable_attr);

  string $children[] = `layout -q -childArray $layout`;
  if( size($children) > 1 )
  {
    int $i;
    for($i = 1; $i < size($children); $i++)
    {
      control -edit -enable $value ($layout + "|" + $children[$i]);
    }
  }
}

global proc AE_SET_compositingModeNew(
  string $enable_attr,
  string $compositingMode_attr)
{
  createControlsForEnumAttr(
    $enable_attr, $compositingMode_attr, "Compositing", "compositingModeLayout");

  AE_SET_compositingModeReplace($enable_attr, $compositingMode_attr);
}

global proc AE_SET_compositingModeReplace(
  string $enable_attr,
  string $compositingMode_attr)
{
  connectControlsForEnumAttr($enable_attr, $compositingMode_attr, "compositingModeLayout");
}

global proc AE_SET_boolNew(
  string $enable_attr, string $attr )
{
  string $tokens[];
  int $i = `tokenize $attr "." $tokens`;

  if( $i != 2 )
    return;

  string $layout = $tokens[1] + "Layout";
  createControlsForBoolAttr( $enable_attr, $attr, $layout );
  AE_SET_boolReplace($enable_attr, $attr);
}

global proc AE_SET_boolReplace(
  string $enable_attr,
  string $attr)
{
  string $tokens[];
  int $i = `tokenize $attr "." $tokens`;

  if( $i != 2 )
    return;

  print $tokens[1];

  string $layout = $tokens[1] + "Layout";
  connectControlsForBoolAttr( $enable_attr, $attr, $layout);
}

global proc AE_SET_transformSamplesNew(string $enable_attr, string $samples_attr)
{
  createControlsForIntAttr(
    $enable_attr, $samples_attr, "Transformation", "transformSamplesLayout");

  AE_SET_transformSamplesReplace($enable_attr, $samples_attr);
}

global proc AE_SET_transformSamplesReplace(
  string $enable_attr,
  string $samples_attr)
{
  connectControlsForIntAttr(
    $enable_attr, $samples_attr, "transformSamplesLayout");
}

global proc AE_SET_deformSamplesNew(string $enable_attr, string $samples_attr)
{
  createControlsForIntAttr(
    $enable_attr, $samples_attr, "Deformation", "deformSamplesLayout");

  AE_SET_deformSamplesReplace($enable_attr, $samples_attr);
}

global proc AE_SET_deformSamplesReplace(
  string $enable_attr,
  string $samples_attr)
{
  connectControlsForIntAttr(
    $enable_attr, $samples_attr, "deformSamplesLayout");
}

global proc AE_SET_separatorNew(string $plug)
{
  DL_separatorWithLabel("samplesSeparator", "Additional Motion Samples", 144);
}

global proc AE_SET_descriptionNew(string $plug)
{
  text -label "" -height 24;
  text
    -label ( "<span style=\"text-align:center;color:#909090\">" +
      "Activating attributes will override their counterpart " +
      "defined on set members." +
      "</span>" )
    -font "smallObliqueLabelFont";
  text -label "" -height 8;
}

global proc AE_SET_dummyReplace(string $plug)
{
}

global proc AEdlSetTemplate(string $node)
{
	editorTemplate -beginScrollLayout;

  AE_addFullLengthSeparatorWithLabel( "topSeparator", "Overrides", 60, 0, 4 );

	editorTemplate -callCustom
		"AE_SET_boolNew" "AE_SET_boolReplace"
		"enablePolyAsSubd" "polyAsSubd"; // "polyAsSubdLayout";

  editorTemplate -callCustom
    "AE_SET_boolNew" "AE_SET_boolReplace"
    "enableSmoothCurves" "smoothCurves"; // "smoothCurvesLayout";

  editorTemplate -callCustom
    "AE_SET_boolNew" "AE_SET_boolReplace"
    "enableCameraVisibility" "cameraVisibility"; // "cameraVisibilityLayout";
  editorTemplate -callCustom
    "AE_SET_boolNew" "AE_SET_boolReplace"
    "enableDiffuseVisibility" "diffuseVisibility"; // "diffuseVisibilityLayout";
  editorTemplate -callCustom
    "AE_SET_boolNew" "AE_SET_boolReplace"
    "enableReflectionVisibility" "reflectionVisibility"; // "reflectionVisibilityLayout";
  editorTemplate -callCustom
    "AE_SET_boolNew" "AE_SET_boolReplace"
    "enableRefractionVisibility" "refractionVisibility"; // "refractionVisibilityLayout";
  editorTemplate -callCustom
    "AE_SET_boolNew" "AE_SET_boolReplace"
    "enableShadowVisibility" "shadowVisibility"; // "shadowVisibilityLayout";

  editorTemplate -callCustom
    "AE_SET_compositingModeNew" "AE_SET_compositingModeReplace"
    "enableCompositingMode" "compositingMode";

  editorTemplate -callCustom "AE_SET_separatorNew" "AE_SET_dummyReplace" "";

  editorTemplate -callCustom
    "AE_SET_transformSamplesNew" "AE_SET_transformSamplesReplace"
    "enableExtraTransformSamples" "extraTransformSamples";

  editorTemplate -callCustom
    "AE_SET_deformSamplesNew" "AE_SET_deformSamplesReplace"
    "enableExtraDeformSamples" "extraDeformSamples";

  editorTemplate -callCustom "AE_SET_descriptionNew" "AE_SET_dummyReplace" "";

  AE_addSpacer( 12 );

  editorTemplate -addExtraControls -extraControlsLabel "";

  editorTemplate -suppress caching;
  editorTemplate -suppress nodeState;
  editorTemplate -suppress frozen;
  editorTemplate -suppress blackBox;
  editorTemplate -suppress rmbCommand;
  editorTemplate -suppress templateName;
  editorTemplate -suppress templatePath;
  editorTemplate -suppress viewName;
  editorTemplate -suppress iconName;
  editorTemplate -suppress viewMode;
  editorTemplate -suppress templateVersion;
  editorTemplate -suppress uiTreatment;
  editorTemplate -suppress customTreatment;
  editorTemplate -suppress creator;
  editorTemplate -suppress creationDate;
  editorTemplate -suppress containerType;
  editorTemplate -suppress dagSetMembers;
  editorTemplate -suppress dnSetMembers;
  editorTemplate -suppress memberWireFrameColor;
  editorTemplate -suppress annotation;
  editorTemplate -suppress isLayer;
  editorTemplate -suppress memberWireframeColor;
  editorTemplate -suppress verticesOnlySet;
  editorTemplate -suppress edgesOnlySet;
  editorTemplate -suppress facetsOnlySet;
  editorTemplate -suppress editPointsOnlySet;
  editorTemplate -suppress renderableOnlySet;
  editorTemplate -suppress editPointsOnlySet;
  editorTemplate -suppress "partition";
  editorTemplate -suppress "enableMatte";
  editorTemplate -suppress "matte";
  editorTemplate -suppress "enablePrelit";
  editorTemplate -suppress "prelit";
	editorTemplate -endScrollLayout;
}
